import { ConfigProvider } from "antd";
import Routes from './Routes';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import viVN from 'antd/es/locale/vi_VN';
import enUS from 'antd/es/locale/en_US';
import { notification } from 'antd';
import { useTranslation } from "react-i18next";
import { useEffect } from "react";

import LocalStorage from "./utils/localStorage";
import { LANGUAGE_LIST, VI_MOMENT_CONFIG } from "./utils/constants";
import userServices from "./services/users.service";
import { saveProfile } from "./redux/actions/profile.actions";


    const localLanguage = LocalStorage.getInstance().read('language') || LANGUAGE_LIST[0]?.value;

    let locale = viVN
    switch (localLanguage) {
        case LANGUAGE_LIST[1]?.value:
            locale = enUS
            break;
        default:
            moment.locale('vi', VI_MOMENT_CONFIG);
            locale = viVN
            break;
    }

    notification.config({
        duration: 10,
    });

    function App() {
        const dispatch = useDispatch();
        const { t } = useTranslation();

        useEffect(() => {
            // initial load
            const accessToken = LocalStorage.getInstance().read('accessToken');
            if (accessToken) {
                getProfile()
            }
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [])

        const getProfile = async () => {
            const resp = await userServices.getProfile();
            const data = resp.data;
            if (resp?.status === 200) {
                let respInfo;
                if (data?.data?.permission === 3) {
                    LocalStorage.getInstance().save('accessToken', null);
                    // notification.error({
                    //     message: t("login.customerPermission")
                    // })
                    return;
                }
                 else if (data?.data?.permission === 1) {
                    respInfo = await userServices.getListAccounts({
                        page: 0,
                        size: 1000,
                        search: JSON.stringify({
                            searchType: 0,
                        }),
                    })
                } else {
                    respInfo = await userServices.getListAccounts({
                        page: 0,
                        size: 1000,
                        search: JSON.stringify({
                            searchType: 1,
                        }),
                    })
                }
                const dataInfo = respInfo?.data;
                if (respInfo?.status === 200) {
                    const userInfo = dataInfo?.data?.content?.find((item: any) => item?.admUser?.id === data?.data?.id)
                    dispatch(saveProfile(userInfo));
                }
            }
        }

        return (
            <ConfigProvider>
                <Routes />
            </ConfigProvider>
        );
    }

    export default App;