import React from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import { useSelector } from 'react-redux';

// import LocalStore from "./utils/localStorage";

import Layout from "./components/Layout";
import Page404 from './containers/Page404';
// import Login from './containers/Login';
import Home from './containers/Home';
import About from './containers/About';
import Contact from './containers/Contact';
import Service from './containers/Service';
import Price from './containers/Price';
import Team from './containers/Team';
import Testimonial from './containers/Testimonial';
import Appointment from './containers/Appointment';
import Profile from './containers/Profile';

const MyRoutes = () => {
    // const accessToken = LocalStore.getInstance().read('accessToken')
    const {profile} = useSelector((state:any) => state?.profileReducer);
    return (
        <Router>
                    <Layout>
                        <Routes>
                            <Route path="/" element={<Home />}/>
                            <Route path="/profile" element={<Profile />}/>

                           
                            <Route path="/about" element={<About />}/>

                          
                            <Route path="/appointment" element={<Appointment />}/>

                       
                             <Route path="/contact" element={<Contact />}/>

                      
                            <Route path="/service" element={<Service />}/>

                      
                            <Route path="/price" element={<Price />}/>

                   
                            <Route path="/Team" element={<Team />}/>

                     
                            <Route path="/Testimonial" element={<Testimonial />}/>

                    
                            <Route path="/appointment" element={<Appointment />}/>

                            {/* <Route path='*' element={<Page404/>} /> */}
                        </Routes>
                    </Layout>
                {/* :
                <Routes>
                    <Route path="/" element={<Login />}/>
                    <Route path='*' element={<Page404/>} />
                </Routes> */}
        </Router>
    );
}
export default MyRoutes
