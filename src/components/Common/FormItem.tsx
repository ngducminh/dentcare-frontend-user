import {Form, FormItemProps} from "antd";
import CommonInput from "./Input";
import React from "react";
import CommonSelect from "./Select";
import CommonDatePicker from "./DatePicker";
import CommonRangePicker from "./RangePicker";
// import {getDOMProps} from "../../utils/utilFunctions";
import CommonSwitch from "./Switch";
import CommonTextArea from './TextArea';
import CommonInputNumber from "./InputNumber";

interface CommonFormProps extends FormItemProps {
    children?: any,
    type?: 'input' | 'select' | 'datePicker' | 'rangePicker' | 'switch' | 'textArea' | 'inputNumber',
    isView?: boolean,
    disabled?: boolean,
    placeholder?: string | string[],
    rules?: any,
    label?: any,
    showRequiredIcon?: boolean,
    maxLength?: number, // for input, textArea
    rows?: number, // for textArea

    min?: number, // for inputNumber
    max?: number, // for inputNumber
    precision?: number, // for inputNumber

    allowClear?: boolean, // for select
    mode?: "multiple" | "tags" | undefined, // for select
    tagRender?: any, // for select
    options?: any[], // for select
    dropdownRender?: any, // for select
    onChange?: any, // callback for select

    disabledDate?: any, // for datepicker
    onSelect?:any, 
    picker?: string,
    format?: string, // for datepicker
    showTime?: any // for datepicker
}

const CommonFormItem = (props:CommonFormProps) => {
    const {
        children,
        className,
        placeholder = '',
        type = 'input',
        options = [],
        isView,
        disabled,
        maxLength,
        rows,
        mode,
        tagRender,
        allowClear = true,
        min,
        max,
        picker,
        precision,
        disabledDate,
        onSelect,
        dropdownRender,
        onChange,
        format,
        showTime
        // showRequiredIcon = false
    } = props;

    const showRequiredIcon = props?.showRequiredIcon || false ;

    const getElementByType = (typeParam:string) => {
        switch (typeParam) {
            case 'select':
                return <CommonSelect
                    allowClear={allowClear}
                    showSearch
                    placeholder={placeholder}
                    disabled={disabled}
                    isView={isView}
                    options={options} //{value: a, label: string}
                    mode={mode}
                    tagRender={tagRender}
                    dropdownRender={dropdownRender}
                    onChange={onChange}
                />
            case 'datePicker':
                return <CommonDatePicker
                    disabled={disabled}
                    isView={isView}
                    placeholder={placeholder}
                    disabledDate={disabledDate}
                    onSelect={onSelect}
                    format={format}
                    showTime={showTime}
                    onChange={onChange}
                    picker={picker}
                />
            case 'rangePicker':
                return <CommonRangePicker
                    disabled={disabled}
                    isView={isView}
                    placeholder={placeholder}
                    disabledDate={disabledDate}
                    format={format}
                    showTime={showTime}
                />
            case 'switch':
                return <CommonSwitch
                    disabled={disabled || isView}
                />
            case 'textArea':
                return <CommonTextArea
                    disabled={disabled}
                    isView={isView}
                    placeholder={placeholder as string}
                    rows={rows as number}
                    maxLength={maxLength}
                />
            case 'inputNumber':
                return <CommonInputNumber
                    disabled={disabled}
                    isView={isView}
                    placeholder={placeholder as string}
                    min={min}
                    max={max}
                    precision={precision}
                />
            default:
                return <CommonInput
                    disabled={disabled}
                    isView={isView}
                    placeholder={placeholder as string}
                    maxLength={maxLength}
                    // allowClear
                />
        }
    }

    const childProps = { ...props };
    delete childProps?.showRequiredIcon;
    delete childProps?.isView;
    delete childProps?.tagRender;
    delete childProps?.rows;
    delete childProps?.mode;
    delete childProps?.maxLength;
    delete childProps?.disabledDate;
    delete childProps?.onSelect;
    delete childProps?.showTime;
    delete childProps?.picker;

    return <Form.Item
        validateTrigger={"onBlur"}
        {...childProps}
        label={
            childProps.label ?
            <span>
                {childProps.label}
                {showRequiredIcon && <span className="required-mark">*</span>}
            </span>
            : null
        }
        className={`avic-form-item ${isView ? 'is-view' : ''} ${className || ''}`}
    >
        {children || getElementByType(type)}
    </Form.Item>
}

export default CommonFormItem;