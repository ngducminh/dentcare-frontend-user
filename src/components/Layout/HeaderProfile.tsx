import React, {useMemo, useState} from 'react';
import {Avatar, Dropdown, Space} from 'antd';
import { Link } from "react-router-dom";
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { UserOutlined } from '@ant-design/icons';

import LocalStorage from "../../utils/localStorage";
import ChangePassword from "../../containers/Login/ChangePassword";


function HeaderProfile() {
    const [openModal, setOpenModal] = useState<boolean>(false);
    const { t } = useTranslation();
    const logout = () => {
        LocalStorage.getInstance().save('accessToken', '');
        window.location.href = '/'
    }
    const {profile} = useSelector((state:any) => state?.profileReducer);

    const itemsDropdownProfile = useMemo(() => [
        {
            key: 'profile',
            label: (
                <Link to={'/profile'}>
                    {t("layout.header.personalInfo")}
                </Link>
            ),
        },
        {
            key: 'changePassword',
            label: (
                <div onClick={()=>setOpenModal(true)}>
                    {t("layout.header.changePassword")}
                </div>
            ),
        },
        {
            key: 'logout',
            label: (
                <div onClick={logout}>
                    {t("layout.header.logout")}
                </div>
            ),
        },
    ], [t])

    return <>
        <Dropdown menu={{ items: itemsDropdownProfile }} trigger={["click"]} className='ms-3'>
            <Space className="cursor-pointer">
                {
                    profile?.admUser?.avatar
                        ? <Avatar className="header-avatar" src={profile?.admUser?.avatar} icon={<UserOutlined />} />
                        : <Avatar className="header-avatar" icon={<UserOutlined />} />
                }
            </Space>
        </Dropdown>
        <ChangePassword 
            openModal={openModal}
            handleOk={()=>setOpenModal(false)}
        />
    </>;
}

export default HeaderProfile;

