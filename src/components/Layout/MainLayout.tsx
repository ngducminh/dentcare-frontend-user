import {Breadcrumb, Layout, Space} from 'antd';
import {Link, useLocation, useNavigate} from "react-router-dom";
import React, {useMemo, ReactNode} from 'react';
import { useTranslation } from 'react-i18next';

import Navbar from './Navbar';
import Footer from './Footer';
import Language from '../Language';
import { isHavePermissionScene } from '../../utils/utilFunctions';
import { useSelector } from 'react-redux';
import HeaderProfile from './HeaderProfile';

const { Header, Content } = Layout;

interface LayoutProps {
    children: ReactNode
}

function MainLayout(props:LayoutProps) {
    const navigate = useNavigate();
    const location = useLocation();
    const { t } = useTranslation();
    
    return (
        <Layout className={'fixed-sidebar full-height-layout'} style={{ minHeight: '100vh' }}>
            <Navbar/>
            <Layout className="fixed-header">
                <Content>
                    <div className="main-layout-background pt-4">
                        {props.children}
                    </div>
                </Content>
            </Layout>
            <Footer />
        </Layout>
    );
}

export default MainLayout;

