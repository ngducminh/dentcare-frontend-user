import React, { useCallback, useState } from "react";
import CustomersCreateModal from "../../containers/Customers/CustomersCreateModal";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useQuery } from "../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, EQUIPMENT_PAGE_STATUS } from "../../utils/constants";
import userServices from "../../services/users.service";
import { Modal, Space, notification } from "antd";
import LoginModal from "../../containers/Login/LoginModal";
import Language from "../Language";
import HeaderProfile from "./HeaderProfile";

function Navbar(props: any) {
    const componentPath = '/customers'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj: any = useQuery();
    const { params = {}, search } = queryObj
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;

    const [data, setData] = useState<any>();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [dataSelected, setDataSelected] = useState<number[]>([]);
    const [isLoadingExport, setIsLoadingExport] = useState(false);

    const navbarStyles: React.CSSProperties = {
        position: "fixed",   // Fix the navbar position
        top: 0,              // Stick it to the top
        width: "100%",       // Span the entire width of the viewport
        zIndex: 1000,        // Ensure it appears above other content
    };

    const [currentCustomer, setCurrentCustomer] = useState<any>(undefined)
    const [expandedRowKeys, setExpandedRowKeys] = useState<number[]>([]);
    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: reload ? 0 : page - 1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
                searchType: 2,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    }, [page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])
    const handleOk = () => {
        getData(true);
        setVisibleModalCreate(false);
        setCurrentCustomer(undefined);
    }
    
    console.log("accessToken",window.localStorage.getItem("accessToken"))
    return (
        <div>
            <nav className="navbar navbar-expand-lg bg-white navbar-light shadow-sm px-5 py-3 py-lg-0" style={navbarStyles}>
                <a href="/" className="navbar-brand p-0">
                    <h1 className="m-0 text-primary"><i className="fa fa-tooth me-2"></i>DentCare</h1>
                </a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <div className="navbar-nav ms-auto py-0">
                        <a href="/" className="nav-item nav-link">{t('navbar.home')}</a>
                        <a href="/about" className="nav-item nav-link">{t('navbar.about')}</a>
                        <a href="/service" className="nav-item nav-link">{t('navbar.service')}</a>
                        <div className="nav-item dropdown">
                            <a href="#" className="nav-link dropdown-toggle" data-bs-toggle="dropdown">{t('navbar.page')}</a>
                            <div className="dropdown-menu m-0">
                                <a href="/price" className="dropdown-item">{t('navbar.price')}</a>
                                <a href="/team" className="dropdown-item">{t('navbar.team')}</a>
                                <a href="/testimonial" className="dropdown-item">{t('navbar.testimonial')}</a>
                                <a href="/appointment" className="dropdown-item">{t('navbar.appointment')}</a>
                            </div>
                        </div>
                        <a href="/contact" className="nav-item nav-link">{t('navbar.contact')}</a>
                    </div>
                    <a href="/appointment" className="btn btn-primary px-4 ms-3">{t('navbar.appointment')}</a>
                    {window.localStorage.getItem("accessToken") =="" ?
                        <>
                            <button className="btn btn-primary px-4 ms-2" onClick={() => { setVisibleModalCreate(true) }}>{t('navbar.login')}</button>
                        </>
                        :
                        <>
                            <HeaderProfile />
                        </>
                    }
                    <Language />
                </div>
            </nav>
            {visibleModalCreate ?
                <LoginModal
                    dataDetail={currentCustomer}
                    modalStatus={visibleModalCreate}
                    handleOk={handleOk}
                    handleCancel={() => { setVisibleModalCreate(false); setCurrentCustomer(undefined); }}
                />
                : <></>
            }
        </div>
    )
}

export default Navbar;

