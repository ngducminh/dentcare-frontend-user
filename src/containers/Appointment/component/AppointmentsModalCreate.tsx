import React, { useCallback, useEffect, useState } from "react";
import { Row, Col, Modal, Form, notification } from "antd";
import { useTranslation } from "react-i18next";
import { useWatch } from "antd/es/form/Form";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { ReactComponent as UpdateIcon } from "../../../resources/images/update_icon.svg";
import { ReactComponent as AddIcon } from "../../../resources/images/plus.svg";
import { ReactComponent as DeleteIcon } from "../../../resources/images/delete.svg";
import { ReactComponent as Logo } from "../../../resources/images/logo_small.svg";

import CommonForm from "../../../components/Common/Form";
import CommonFormItem from "../../../components/Common/FormItem";
import CommonButton from "../../../components/Common/Button";
import {
  APPOINTMENT_PAGE_STATUS,
  DATE_TIME_FORMAT,
} from "../../../utils/constants";
import { isDoctorMedical } from "../../../utils/utilFunctions";

import appointmentServices from "../../../services/appointments.service";
import userServices from "../../../services/users.service";
import serviceServices from "../../../services/medicalServices.service";
import medicalServices from "../../../services/medicals.service";
import sendMailServices from "../../../services/sendMails.service";
import LocalStorage from "../../../utils/localStorage";
import { saveProfile } from "../../../redux/actions/profile.actions";

interface AppointmentsModalCreateProps {
  dataDetail: any;
  modalStatus: boolean;
  handleOk: () => void;
  handleCancel: () => void;
}

function AppointmentsModalCreate(props: AppointmentsModalCreateProps) {
  const { modalStatus, handleOk, handleCancel, dataDetail } = props;
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isLoadingMedical, setIsLoadingMedical] = useState<boolean>(false);
  const [dataDoctor, setDataDoctor] = useState<any[]>([]);
  const [dataCustomer, setDataCustomer] = useState<any[]>([]);
  const [dataService, setDataService] = useState<any[]>([]);
  const [selectedDate, setSelectedDate] = useState();
  const [listAppointment, setListAppointment] = useState<string[]>([]);
  const customerId = useWatch("customer", form);
  const doctorId = useWatch("doctor", form);
  const { profile } = useSelector((state: any) => state?.profileReducer);
  const dispatch = useDispatch();

  const getDataDoctor = async () => {
    const paramsSearch = {
      page: 0,
      size: 1000,
      search: JSON.stringify({
        searchType: 0,
      }),
    };
    const resp = await userServices.getListDoctor(paramsSearch);
    const data = resp?.data;
    if (resp?.status === 200) {
      setDataDoctor(data?.data?.content);
      console.log("bbb",data);
    } else {
      setDataDoctor([]);
    }
  };

  // const getDataCustomer = async () => {
  //     const paramsSearch = {
  //         page: 0,
  //         size: 1000,
  //         search: JSON.stringify({
  //             searchType: 2,
  //         }),
  //     }
  //     const resp = await userServices.getListAccounts(paramsSearch);
  //     const data = resp?.data;
  //     if (resp?.status === 200) {
  //         setDataCustomer(data?.data?.content)
  //     } else {
  //         setDataCustomer([])
  //     }
  // }

  const getDataService = async () => {
    const paramsSearch = {
      page: 0,
      size: 1000,
      search: JSON.stringify({}),
    };
    const resp = await serviceServices.getListServices(paramsSearch);
    const data = resp?.data;
    if (resp?.status === 200) {
      setDataService(data?.data);
      console.log("aaa",data);
    } else {
      setDataService([]);
    }
  };

  const getListAppointment = useCallback(async () => {
    if (doctorId) {
      const paramsSearch = {
        page: 0,
        size: 1000,
        search: JSON.stringify({ doctorId: doctorId }),
      };
      const resp = await appointmentServices.getListAppointments(paramsSearch);
      const data = resp?.data;
      if (resp?.status === 200) {
        setListAppointment(
          data?.data?.content
            ?.filter(
              (item: any) =>
                (item?.status === 0 ||
                  item?.status === 1 ||
                  item?.status === 2) &&
                item?.id !== dataDetail?.id
            )
            ?.map((item: any) =>
              moment(item?.bookingDate).format(DATE_TIME_FORMAT)
            )
        );
      } else {
        setListAppointment([]);
      }
    } else {
      setListAppointment([]);
    }
  }, [dataDetail?.id, doctorId]);

  useEffect(() => {
    getListAppointment();
  }, [getListAppointment]);

  useEffect(() => {
    getDataDoctor();
    // getDataCustomer();
    getDataService();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const currentCustomer = dataCustomer?.find(
      (item: any) => item?.id === customerId
    );
    form.setFieldsValue({
      email: currentCustomer?.email,
      phoneNumber: currentCustomer?.phoneNumber,
    });
  }, [customerId, dataCustomer, form]);

  const onChangeDate = (values:any) => {
    console.log(moment(values).format(DATE_TIME_FORMAT),listAppointment)

    const now = moment(values)
    const there = moment(listAppointment[0], DATE_TIME_FORMAT)
    console.log('values ', moment(values).toDate());
    console.log('values 1 ',  moment(listAppointment[0], DATE_TIME_FORMAT).toDate());
    console.log('date diff ', moment.duration(now.diff(there)).asHours());

    for(var i =0; i<listAppointment.length; i++) {
        const first = moment(listAppointment[i], DATE_TIME_FORMAT)
        const second = moment(listAppointment[i+1], DATE_TIME_FORMAT)

        console.log('diff 1 ', i + " " + moment.duration(now.diff(first)).asHours() )
        console.log('diff 2 ', i + " " + Math.abs(moment.duration(now.diff(second)).asHours()))

        if(moment.duration(now.diff(first)).asHours() < 1 && Math.abs(moment.duration(now.diff(second)).asHours()) < 1){
            form.setFields([{
                name: "bookingDate",
                errors: [`${t("appointmentPage.form.warning")}!`],
            }]);
            return;
        }
    }
    
    // console.log('aaaa',  values) - moment(listAppointment[0]));
    // if(listAppointment.includes(moment(values).format(DATE_TIME_FORMAT))){
    //     form.setFields([{
    //         name: "bookingDate",
    //         errors: [`${t("appointmentPage.form.warning")}!`],
    //     }]);
    // }
}

//Check trùng lịch hẹn
const onFinish =  (values:any) => {
    // if(listAppointment.includes(moment(values?.bookingDate).format(DATE_TIME_FORMAT))){
    //     form.setFields([{
    //         name: "bookingDate",
    //         errors: [`${t("appointmentPage.form.warning")}!`],
    //     }]);
    //     return;
    // }
    let check = true;
    const now = moment(values?.bookingDate)
    const there = moment(listAppointment[0], DATE_TIME_FORMAT)
    console.log('values ', moment(values).toDate());
    console.log('values 1 ',  moment(listAppointment[0], DATE_TIME_FORMAT).toDate());
    console.log('date diff ', moment.duration(now.diff(there)).asHours());

    for(var i =0; i<listAppointment.length; i++) {
        const first = moment(listAppointment[i], DATE_TIME_FORMAT)
        const second = moment(listAppointment[i+1], DATE_TIME_FORMAT)
        console.log('update')

        console.log('diff 1 ', i + " " + moment.duration(now.diff(first)).asHours() )
        console.log('diff 2 ', i + " " + Math.abs(moment.duration(now.diff(second)).asHours()))

        if(Math.abs(moment.duration(now.diff(first)).asHours()) < 1 || Math.abs(moment.duration(now.diff(second)).asHours()) < 1){
            form.setFields([{
                name: "bookingDate",
                errors: [`${t("appointmentPage.form.warning")}!`],
            }]);
            check = false;
            console.log("vao day");
            return;
        }
    }
    if(check) {
        console.log('add');
        onCreateAppointment(values);
    }
}

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  const onCreateAppointment = async (values: any) => {
    setIsLoading(true);
    const dataSubmit = { ...values };
    dataSubmit["admCustomer"] = { id: profile.id };
    dataSubmit["admDoctor"] = { id: values?.doctor };
    // dataSubmit["admStaff"] = { id: profile?.id};
    dataSubmit["services"] = values?.services?.map((item: any) => ({
      id: item,
    }));
    delete dataSubmit?.customer;
    delete dataSubmit?.doctor;
    delete dataSubmit?.email;
    delete dataSubmit?.phoneNumber;
    if (dataDetail?.id) {
      if (!dataDetail?.admStaff?.id) {
        dataSubmit["admStaff"] = { id: profile?.id };
      }
      const resp = await appointmentServices.updateAppointment({
        ...dataDetail,
        ...dataSubmit,
      });
      const data = resp?.data;
      if (resp?.status === 200) {
        notification.success({
          message: t("appointmentPage.message.editSuccess"),
        });
        handleOk();
      } else {
        notification.error({
          message: data?.message || t("commonError.oopsSystem"),
        });
      }
    } else {
      dataSubmit["status"] = 0;
      console.log("values", dataSubmit);
      const resp = await appointmentServices.createAppointment(dataSubmit);
      const data = resp?.data;
      if (resp?.status === 200) {
        notification.success({
          message: t("appointmentPage.message.createSuccess"),
        });
        sendMailServices.createSendMail({
          recipient: form.getFieldValue("email"),
          subject: t("sendEmail.title"),
          msgBody: `
                        ${t(
                          "sendEmail.createAppointment"
                        )} ${form.getFieldValue("appointmentCode")}
                        ${t("sendEmail.time")} ${moment(
            form.getFieldValue("bookingDate")
          ).format(DATE_TIME_FORMAT)}
                        ${t("sendEmail.address")}
                        ${t("sendEmail.thankyou")}
                    `,
        });
        const currentDoctor = dataDoctor?.find(
          (item: any) => item?.id === form.getFieldValue("doctor")
        );
        const currentCustomer = dataCustomer?.find(
          (item: any) => item?.id === form.getFieldValue("customer")
        );
        if (currentDoctor && currentCustomer) {
          sendMailServices.createSendMail({
            recipient: currentDoctor?.email,
            subject: t("sendEmail.title"),
            msgBody: `
                            ${t("sendEmail.doctor")} ${
              currentCustomer?.fullName
            } (${currentCustomer?.customerCode})
                            ${t("sendEmail.appointment")} ${form.getFieldValue(
              "appointmentCode"
            )}
                            ${t("sendEmail.time")} ${moment(
              form.getFieldValue("bookingDate")
            ).format(DATE_TIME_FORMAT)}
                            ${t("sendEmail.address")}
                        `,
          });
        }
        handleOk();
      } else {
        notification.error({
          message: data?.message || t("commonError.oopsSystem"),
        });
      }
    }
    setIsLoading(false);
  };

  const onCancelAppointment = async (newStatus: number) => {
    setIsLoading(true);
    let check: boolean = false;
    if (newStatus === 1) {
      if (!form.getFieldValue("doctor")) {
        form.setFields([
          {
            name: "doctor",
            errors: [
              `${t("validate.select")} ${t(
                "appointmentPage.form.label.doctor"
              )}!`,
            ],
          },
        ]);
        check = true;
      }
      if (!form.getFieldValue("services")?.length) {
        form.setFields([
          {
            name: "services",
            errors: [
              `${t("validate.select")} ${t(
                "appointmentPage.form.label.service"
              )}!`,
            ],
          },
        ]);
        check = true;
      }
    }
    if (!check) {
      Modal.confirm({
        title: t("common.confirmAction"),
        content: t("common.editDataAction"),
        centered: true,
        okText: t("common.button.accept"),
        onOk: async () => {
          const dataSubmit = {
            ...dataDetail,
            status: newStatus,
            admDoctor: { id: form.getFieldValue("doctor") },
            services: form
              .getFieldValue("services")
              ?.map((item: any) => ({ id: item })),
          };
          const resp = await appointmentServices.updateAppointment(dataSubmit);
          const data = resp?.data;
          if (resp?.status === 200) {
            if (newStatus === 4) {
              notification.success({
                message: t("appointmentPage.message.cancelSuccess"),
              });
              sendMailServices.createSendMail({
                recipient: dataDetail?.admCustomer?.email,
                subject: t("sendEmail.title"),
                msgBody: `
                                    ${t("sendEmail.cancelAppointment")}
                                    ${t("sendEmail.appointment")} ${
                  dataDetail?.appointmentCode
                }
                                    ${t("sendEmail.time")} ${moment(
                  dataDetail?.bookingDate
                ).format(DATE_TIME_FORMAT)}
                                    ${t("sendEmail.address")}
                                    ${t("sendEmail.thankyou")}
                                `,
              });
              sendMailServices.createSendMail({
                recipient: dataDetail?.admDoctor?.email,
                subject: t("sendEmail.title"),
                msgBody: `
                                    ${t("sendEmail.doctorCancel")} ${
                  dataDetail?.admCustomer?.fullName
                } (${dataDetail?.admCustomer?.customerCode}) ${t(
                  "sendEmail.cancel"
                )}
                                    ${t("sendEmail.appointment")} ${
                  dataDetail?.appointmentCode
                }
                                    ${t("sendEmail.time")} ${moment(
                  dataDetail?.bookingDate
                ).format(DATE_TIME_FORMAT)}
                                    ${t("sendEmail.address")}
                                `,
              });
            } else if (newStatus === 1) {
              notification.success({
                message: t("appointmentPage.message.approvalSuccess"),
              });
            }
            handleOk();
          } else {
            notification.error({
              message: data?.message || t("commonError.oopsSystem"),
            });
          }
        },
        okButtonProps: { loading: isLoading },
        cancelText: t("common.button.cancel"),
      });
    }

    setIsLoading(false);
  };

  const onCreateMedical = () => {
    setIsLoading(true);
    Modal.confirm({
      title: t("common.confirmAction"),
      content: t("common.editDataAction"),
      centered: true,
      okText: t("common.button.accept"),
      onOk: async () => {
        setIsLoadingMedical(true);
        const dataCreate = {
          admAppointment: {
            id: dataDetail?.id,
          },
          medicalCode: `DentCare_MDC_${moment().unix()}`,
          medicalDate: moment().toISOString(),
          diagnostic: "",
          note: "",
          prescription: "",
        };
        console.log(dataCreate);
        const resp = await medicalServices.createMedical(dataCreate);
        const data = resp?.data;
        if (resp?.status === 200) {
          const respAppointment = await appointmentServices.updateAppointment({
            ...dataDetail,
            status: 2,
          });
          if (respAppointment?.status === 200) {
            notification.success({
              message: t("medicalPage.message.createSuccess"),
            });
            navigate(`/medicals/detail/${data?.data?.id}`, { replace: true });
          } else {
            notification.error({
              message: data?.message || t("commonError.oopsSystem"),
            });
          }
        } else {
          notification.error({
            message: data?.message || t("commonError.oopsSystem"),
          });
        }
        setIsLoadingMedical(false);
      },
      okButtonProps: { loading: isLoadingMedical },
      cancelText: t("common.button.cancel"),
    });
    setIsLoading(false);
  };

  useEffect(() => {
    // initial load
    const accessToken = LocalStorage.getInstance().read("accessToken");
    if (accessToken) {
      getProfile();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getProfile = async () => {
    const resp = await userServices.getProfile();
    const data = resp.data;
    if (resp?.status === 200) {
      let respInfo;
      if (data?.data?.permission !== 3) {
        LocalStorage.getInstance().save("accessToken", null);
        notification.error({
          message: t("login.customerPermission"),
        });
        return;
      } else if (data?.data?.permission === 1) {
        respInfo = await userServices.getListAccounts({
          page: 0,
          size: 1000,
          search: JSON.stringify({
            searchType: 0,
          }),
        });
      } else {
        respInfo = await userServices.getListAccounts({
          page: 0,
          size: 1000,
          search: JSON.stringify({
            searchType: 1,
          }),
        });
      }
      const dataInfo = respInfo?.data;
      if (respInfo?.status === 200) {
        const userInfo = dataInfo?.data?.content?.find(
          (item: any) => item?.admUser?.id === data?.data?.id
        );
        dispatch(saveProfile(userInfo));
      }
    }
  };

  return (
    <Modal
      className="appointment-modal-create"
      title={
        <>
          <div className="text-center">
            <Logo className="style-size-logo" />
          </div>
          <h2 className="txt-title text-center">Dentcare</h2>
        </>
      }
      open={modalStatus}
      onCancel={handleCancel}
      maskClosable={false}
      footer={[
        <CommonButton key="close" onClick={handleCancel} size="small">
          {t("common.button.close")}
        </CommonButton>,
        (dataDetail?.status === 0 || dataDetail?.status === 1) &&
        profile?.admUser?.permission !== 1 ? (
          <CommonButton
            key="cancel"
            onClick={() => onCancelAppointment(4)}
            btnType="danger"
            size="small"
            className="btn-icon-left"
          >
            <>
              <DeleteIcon /> {t("common.button.cancel")}
            </>
          </CommonButton>
        ) : null,
        dataDetail?.status === 1 &&
        (isDoctorMedical(dataDetail?.admDoctor?.id, profile) ||
          profile?.admUser?.permission !== 1) ? (
          <CommonButton
            key="createMedical"
            onClick={onCreateMedical}
            btnType="info"
            size="small"
            className="btn-icon-left"
          >
            <>
              <AddIcon /> {t("common.button.createMedical")}
            </>
          </CommonButton>
        ) : null,
        dataDetail?.status === 0 && profile?.admUser?.permission !== 1 ? (
          <CommonButton
            key="confirmMedical"
            onClick={() => onCancelAppointment(1)}
            btnType="success"
            size="small"
            className="btn-icon-left"
          >
            <>
              <AddIcon /> {t("common.button.approval")}
            </>
          </CommonButton>
        ) : null,
        dataDetail?.status === 0 && profile?.admUser?.permission !== 1 ? (
          <CommonButton
            form="myForm"
            key="update"
            htmlType="submit"
            btnType="primary"
            size="small"
            className="btn-icon-left"
            loading={isLoading}
          >
            <>
              <UpdateIcon /> {t("common.button.update")}
            </>
          </CommonButton>
        ) : null,
        !dataDetail?.id ? (
          <CommonButton
            form="myForm"
            key="submit"
            htmlType="submit"
            btnType="primary"
            size="small"
            className="btn-icon-left"
            loading={isLoading}
            style={{ background: "#06A3DA", borderColor: "#06A3DA" }}
          >
            <>
              <AddIcon /> {t("common.button.addNew")}
            </>
          </CommonButton>
        ) : null,
      ]}
    >
      <CommonForm
        form={form}
        id="myForm"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        layout="vertical"
        initialValues={{
          appointmentCode: dataDetail?.appointmentCode
            ? dataDetail?.appointmentCode
            : `DentCare_APP_${moment().unix()}`,
          customer: dataDetail?.admCustomer?.id,
          doctor: dataDetail?.admDoctor?.id,
          services: dataDetail?.services?.map((item: any) => item?.id),
          bookingDate: dataDetail?.bookingDate
            ? moment(dataDetail?.bookingDate)
            : undefined,
          note: dataDetail?.note,
          status: dataDetail?.status,
        }}
        disabled={
          (dataDetail?.status === 0 || !dataDetail?.id) &&
          profile?.admUser?.permission !== 1
            ? false
            : true
        }
      >
        <Row gutter={20}>
          <Col span={12}>
            <CommonFormItem
              label={t("appointmentPage.form.label.appointmentCode") as string}
              name="appointmentCode"
              placeholder={
                t("appointmentPage.form.placeholder.appointmentCode") as string
              }
              disabled
            />
          </Col>
          {/* <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.customer") as string} 
                        name="customer"
                        placeholder={t("appointmentPage.form.placeholder.customer") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("appointmentPage.form.label.customer")}!` }
                        ]}
                        type="select"
                        options={dataCustomer?.map((item:any)=>({value: item?.id, label: item?.fullName}))}
                        showRequiredIcon
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.email") as string} 
                        name="email"
                        placeholder={t("appointmentPage.form.placeholder.email") as string}
                        disabled={true}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.phoneNumber") as string}
                        name="phoneNumber"
                        placeholder={t("appointmentPage.form.placeholder.phoneNumber") as string}
                        disabled={true}
                        maxLength={10}
                    />
                </Col> */}
          <Col span={12}>
            <CommonFormItem
              label={t("appointmentPage.form.label.doctor") as string}
              name="doctor"
              placeholder={
                t("appointmentPage.form.placeholder.doctor") as string
              }
              rules={[
                {
                  required: true,
                  message: `${t("validate.select")} ${
                    t("appointmentPage.form.label.doctor"
                  )}!`,
                },
              ]}
              showRequiredIcon
              type="select"
              options={dataDoctor?.map((item: any) => ({
                value: item?.id,
                label: item?.fullName,
              }))}
            />
          </Col>
          <Col span={12}>
            <CommonFormItem
              label={t("appointmentPage.form.label.service") as string}
              name="services"
              placeholder={
                t("appointmentPage.form.placeholder.service") as string
              }
              showRequiredIcon
              type="select"
              mode="multiple"
              options={dataService?.map((item: any) => ({
                value: item?.id,
                label: item?.name,
              }))}
              rules={[
                {
                  required: true,
                  message: `${t("validate.select")} ${t(
                    "appointmentPage.form.label.service"
                  )}!`,
                },
              ]}
            />
          </Col>
          <Col span={12}>
            <Row>
              <Col span={24}>
                <CommonFormItem
                  format={DATE_TIME_FORMAT}
                  showTime={{
                    disabledHours: () => {
                      let listDisableHours: number[] = [
                        0, 1, 2, 3, 4, 5, 6, 22, 23,
                      ];
                      if (moment(selectedDate).unix() <= moment().unix()) {
                        listDisableHours = [
                          ...listDisableHours,
                          ...Array.from(Array(moment().hours()).keys()),
                        ];
                      }
                      return listDisableHours;
                    },
                    disabledMinutes: () =>
                      moment(selectedDate).unix() <= moment().unix()
                        ? Array.from(Array(moment().minutes()).keys())
                        : [],
                  }}
                  showRequiredIcon={true}
                  label={t("appointmentPage.form.label.bookingDate")}
                  name="bookingDate"
                  type="datePicker"
                  disabledDate={(current: any) =>
                    current < moment().startOf("day").valueOf()
                  }
                  onSelect={setSelectedDate}
                  placeholder={
                    t("appointmentPage.form.placeholder.bookingDate") as string
                  }
                  rules={[
                    {
                      required: true,
                      message: `${t("validate.select")} ${t(
                        "appointmentPage.form.label.bookingDate"
                      )}!`,
                    },
                  ]}
                  onChange={onChangeDate}
                />
              </Col>
              {dataDetail?.id ? (
                <Col span={24}>
                  <CommonFormItem
                    label={t("appointmentPage.form.label.status") as string}
                    name="status"
                    type="select"
                    options={APPOINTMENT_PAGE_STATUS?.map((item: any) => ({
                      value: item.value,
                      label: t(item.label),
                    }))}
                    disabled={true}
                  />
                </Col>
              ) : (
                <></>
              )}
            </Row>
          </Col>
          {/* <Col span={12}>
                    <CommonFormItem 
                        label={t("appointmentPage.form.label.note") as string}
                        name="note" 
                        type="textArea"
                        rows={5}
                        placeholder={t("appointmentPage.form.placeholder.note") as string}
                    />
                </Col> */}
        </Row>
      </CommonForm>
    </Modal>
  );
}

export default AppointmentsModalCreate;
