import React, { useCallback, useState } from "react";
import CustomersCreateModal from "../../containers/Customers/CustomersCreateModal";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useQuery } from "../../utils/customHooks";
import { DATE_TIME_FORMAT_SECOND, DEFAULT_PAGE_SIZE, EQUIPMENT_PAGE_STATUS } from "../../utils/constants";
import userServices from "../../services/users.service";
import { Modal, Space, notification } from "antd";
import AppointmentsModalCreate from "./component/AppointmentsModalCreate";

function Appointment() {
    const componentPath = '/customers'
    const navigate = useNavigate();
    const { t } = useTranslation();
    const queryObj: any = useQuery();
    const { params = {}, search } = queryObj
    const {
        page: pageQuery,
        pageSize: pageSizeQuery,
        sortBy: sortByQuery,
        sortType: sortTypeQuery,
        search: searchQuery,
    } = params
    const page = pageQuery ? parseFloat(pageQuery) : 1;
    const pageSize = pageSizeQuery ? parseFloat(pageSizeQuery) : DEFAULT_PAGE_SIZE;

    const [data, setData] = useState<any>();
    const [visibleModalCreate, setVisibleModalCreate] = useState<boolean>(false);
    const [visibleModalAppointment, setVisibleModalAppointment] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState(false);
    const [dataSelected, setDataSelected] = useState<number[]>([]);
    const [isLoadingExport, setIsLoadingExport] = useState(false);
    const [currentCustomer, setCurrentCustomer] = useState<any>(undefined)
    const [currentAppointment, setCurrentAppointment] = useState<any>(undefined)
    const [expandedRowKeys, setExpandedRowKeys] = useState<number[]>([]);
    
    const getData = useCallback(async (reload?: boolean) => {
        setIsLoading(true);
        const newSearch = searchQuery ? JSON.parse(searchQuery) : {};
        const paramsSearch = {
            page: reload ? 0 : page - 1,
            size: pageSize,
            sortBy: sortByQuery,
            sortType: sortTypeQuery,
            search: JSON.stringify({
                ...newSearch,
                searchType: 2,
            }),
        }
        const resp = await userServices.getListAccounts(paramsSearch);
        const data = resp?.data;
        if (resp?.status === 200) {
            setData(data?.data)
        } else {
            notification.error({
                message: data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    }, [page, pageSize, searchQuery, sortByQuery, sortTypeQuery, t])
    
    const handleOk = () => {
        // getData(true);
        setVisibleModalCreate(false);
        setCurrentCustomer(undefined);
        setVisibleModalAppointment(false);
        setCurrentAppointment(undefined);
    }

    const handleResigterOk = () => {
        setVisibleModalCreate(false);
        setCurrentCustomer(undefined);
        setVisibleModalAppointment(false);
        setCurrentAppointment(undefined);
    }

    return (
        <div>

            <div className="container-fluid bg-primary py-5 hero-header mb-5 mt-5">
                <div className="row py-3">
                    <div className="col-12 text-center">
                        <h1 className="display-3 text-white animated zoomIn">Appointment</h1>
                        <a href="/" className="h4 text-white">Home</a>
                        <i className="far fa-circle text-white px-2"></i>
                        <a href="/appointment" className="h4 text-white">Appointment</a>
                    </div>
                </div>
            </div>
            <div className="container-fluid bg-primary bg-appointment mb-5 wow fadeInUp" data-wow-delay="0.1s" style={{ marginTop: '90px' }}>
                <div className="container">
                    <div className="row gx-5">
                        <div className="col-lg-6 py-5">
                            <div className="py-5">
                                <h1 className="display-5 text-white mb-4">We Are A Certified and Award Winning Dental Clinic You Can Trust</h1>
                                <p className="text-white mb-0">Eirmod sed tempor lorem ut dolores. Aliquyam sit sadipscing kasd ipsum. Dolor ea et dolore et at sea ea at dolor, justo ipsum duo rebum sea invidunt voluptua. Eos vero eos vero ea et dolore eirmod et. Dolores diam duo invidunt lorem. Elitr ut dolores magna sit. Sea dolore sanctus sed et. Takimata takimata sanctus sed.</p>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="appointment-form h-100 d-flex flex-column justify-content-center text-center p-5 wow zoomIn" data-wow-delay="0.6s">
                                <h1 className="text-white mb-4">Make Appointment</h1>
                                {!window.localStorage.getItem("accessToken") ?
                                    <>
                                        <div className="row g-3">
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Sign up & Log in
                                                </h5>
                                            </div>
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Appointment
                                                </h5>
                                            </div>
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Staff call to confirm
                                                </h5>
                                            </div>
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Visit our facility
                                                </h5>
                                            </div>
                                            <div className="col-12">
                                                <button type="button" className="btn btn-dark w-100 py-3" onClick={() => { setVisibleModalCreate(true) }}>Sign Up Here</button>
                                            </div>
                                        </div>
                                    </>
                                    :
                                    <>
                                        <div className="row g-3">
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Sign up & Log in
                                                </h5>
                                            </div>
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Appointment
                                                </h5>
                                            </div>
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Staff call to confirm
                                                </h5>
                                            </div>
                                            <div className="col-12 col-sm-6">
                                                <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                                    <i className="fa fa-check-circle text-primary me-2"></i>Visit our facility
                                                </h5>
                                            </div>
                                            <div className="col-12">
                                                <button type="button" className="btn btn-dark w-100 py-3" onClick={() => { setVisibleModalAppointment(true) }}>Make Appointment</button>
                                            </div>
                                        </div>
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            {visibleModalCreate ?
                <CustomersCreateModal
                    dataDetail={currentCustomer}
                    modalStatus={visibleModalCreate}
                    handleOk={handleResigterOk}
                    handleCancel={() => { setVisibleModalCreate(false); setCurrentCustomer(undefined); }}
                />
                : <></>
            }

            {visibleModalAppointment ?
                <AppointmentsModalCreate
                    dataDetail={currentAppointment}
                    modalStatus={visibleModalAppointment}
                    handleOk={handleOk}
                    handleCancel={() => { setVisibleModalAppointment(false); setCurrentAppointment(undefined); }}
                />
                : <></>
            }
        </div>
    )
}

export default Appointment;
