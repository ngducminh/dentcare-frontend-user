import React, { useState } from 'react'
import CommonForm from '../../components/Common/Form';
import { useTranslation } from 'react-i18next';
import { Avatar, Col, Input, Row, Upload } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import ImgCrop from 'antd-img-crop';

import {ReactComponent as CameraIcon} from "../../resources/images/camera-icon.svg";
import CommonFormItem from '../../components/Common/FormItem';
import { CUSTOMER_PAGE_PERMISSION, CUSTOMER_PAGE_STATUS, REGEX_PASSWORD } from '../../utils/constants';



export interface AccountInfoTabProps{
    dataDetail: any,
    formAccountInfo: any,
    onCreateAccount: any
}

function AccountInfoTab(props: AccountInfoTabProps) {
    const { t } = useTranslation();
    const { dataDetail, formAccountInfo, onCreateAccount } = props;
    const [fileAvatar, setFileAvatar] = useState<any>(dataDetail?.avatar ? dataDetail?.avatar : null);

    const onFinishAccountInfo =  (values:any) => {
        if(fileAvatar){
            formAccountInfo.setFieldValue("avatar",fileAvatar)
        }else{
            formAccountInfo.setFieldValue("avatar", "")
        }
        onCreateAccount(true);
    }

    const onFinishAccountInfoFailed = (errorInfo:any) => {
        onCreateAccount(false)
        console.log('Failed:', errorInfo);
    };

    const onUploadFile = async (options: any) => {
        if (!options?.file) {
            setFileAvatar(undefined);
            return
        }
        
        console.log("options?.file", options?.file)
        const reader = new FileReader()
        reader.readAsDataURL(options?.file)
        reader.onload = () => {
            console.log('called: ', reader)
            setFileAvatar(reader.result);
        }
        
    }

    return (
        <CommonForm
            form={formAccountInfo}
            onFinish={onFinishAccountInfo}
            onFinishFailed={onFinishAccountInfoFailed}
            layout="vertical"
            initialValues={{
                username: dataDetail?.username,
                password: dataDetail?.password,
                status: dataDetail?.status,
                permission: dataDetail ? dataDetail?.permission : 3, //Khách hàng
            }}
        >
            <Row gutter={20}>
                <Col span={24}>
                <CommonFormItem name="avatar" >
                    <div className="avatar-box-container" style={{paddingTop: 2}}>
                        <div className="avatar-box">
                            {fileAvatar
                                ? <Avatar className="avatar" src={fileAvatar} icon={<UserOutlined />} />
                                :
                                <Avatar className="avatar" icon={<UserOutlined />} />
                            }
                            <div className="avatar-change-icon">
                                <ImgCrop showGrid rotationSlider aspectSlider showReset>
                                    <Upload
                                        fileList={[]}
                                        customRequest={onUploadFile}
                                    >
                                        <CameraIcon />
                                    </Upload>
                                </ImgCrop>
                            </div>
                        </div>
                    </div>
                </CommonFormItem>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.userName") as string} 
                        name="username"
                        placeholder={t("customerPage.form.placeholder.userName") as string}
                        showRequiredIcon={!dataDetail?.id} 
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("customerPage.form.label.userName")}!` }
                        ]}
                        disabled={dataDetail?.id}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem
                        validateTrigger={["onChange", "onBlur"]}
                        name="password"
                        label={t("customerPage.form.label.password") as string} 
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("customerPage.form.label.password")}!` },
                            { pattern: REGEX_PASSWORD, message: `${t("validate.passwordRegex")}` }
                        ]}
                        disabled={dataDetail?.id} 
                        showRequiredIcon={!dataDetail?.id} 
                    >
                        <Input.Password 
                            disabled={dataDetail?.id} 
                            type="password" 
                            placeholder={t("customerPage.form.placeholder.password") as string} 
                            allowClear 
                        />
                    </CommonFormItem>
                </Col> 
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.permission") as string} 
                        name="permission"
                        type='select'
                        options={CUSTOMER_PAGE_PERMISSION?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                        disabled
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.status") as string}
                        name="status" 
                        placeholder={t("customerPage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("customerPage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={CUSTOMER_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
            </Row>
        </CommonForm>
    )
}

export default AccountInfoTab