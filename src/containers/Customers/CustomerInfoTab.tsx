import React from 'react'
import {Row, Col, Radio} from 'antd';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

import { DATE_FORMAT, DOCTOR_PAGE_STATUS, REGEX_EMAIL, REGEX_PHONE_NUMBER } from '../../utils/constants';
import CommonForm from '../../components/Common/Form';
import CommonFormItem from '../../components/Common/FormItem';

export interface CustomerInfoTabProps{
    dataDetail: any,
    formCustomerInfo: any,
    onCreateCustomer: any
}

function CustomerInfoTab(props: CustomerInfoTabProps) {
    const { t } = useTranslation();
    const { dataDetail, formCustomerInfo, onCreateCustomer } = props;

    const onFinishCustomerInfo =  (values:any) => {
        onCreateCustomer(true);
    }

    const onFinishCustomerInfoFailed = (errorInfo:any) => {
        onCreateCustomer(false);
        console.log('Failed:', errorInfo);
    };

    return (
        <CommonForm
            form={formCustomerInfo}
            onFinish={onFinishCustomerInfo}
            onFinishFailed={onFinishCustomerInfoFailed}
            layout="vertical"
            initialValues={{
                customerCode: dataDetail?.customerCode ? dataDetail?.customerCode : `KH_${moment().unix()}`,
                fullName: dataDetail?.fullName,
                dob: dataDetail?.dob ? moment(dataDetail?.dob) : undefined,
                gender: dataDetail ? dataDetail?.gender : 0,
                email: dataDetail?.email,
                phoneNumber: dataDetail?.phoneNumber,
                address: dataDetail?.address,
                status: dataDetail?.status,
                note: dataDetail?.note,
            }}
        >
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.customerCode") as string} 
                        name="customerCode"
                        placeholder={t("customerPage.form.placeholder.customerCode") as string}
                        disabled
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.fullName") as string} 
                        name="fullName"
                        placeholder={t("customerPage.form.placeholder.fullName") as string}
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("customerPage.form.label.fullName")}!` }
                        ]}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.dob") as string} 
                        name="dob"
                        placeholder={t("customerPage.form.placeholder.dob") as string}
                        type='datePicker'
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("customerPage.form.label.dob")}!` }
                        ]}
                        disabledDate={(current:any)=> current >= moment().endOf("day").toDate()}
                        format={DATE_FORMAT}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem
                        name="gender"
                        label={t('customerPage.form.label.gender')}
                        showRequiredIcon
                    >
                        <Radio.Group >
                            <Radio value={0}>{t("customerPage.options.gender.male")}</Radio>
                            <Radio value={1}>{t("customerPage.options.gender.female")}</Radio>
                        </Radio.Group>
                    </CommonFormItem>
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.email") as string}
                        name="email"
                        placeholder={t("customerPage.form.placeholder.email") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("customerPage.form.label.email")}!`},
                            { pattern: REGEX_EMAIL, message: `${t("validate.emailRegex")}`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.phoneNumber") as string}
                        name="phoneNumber"
                        placeholder={t("customerPage.form.placeholder.phoneNumber") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("customerPage.form.label.phoneNumber")}!`},
                        { pattern: new RegExp(REGEX_PHONE_NUMBER), message: t('validate.phoneNumberFormat') as string },
                    ]}
                        maxLength={10}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.address") as string}
                        name="address"
                        placeholder={t("customerPage.form.placeholder.address") as string}  
                        showRequiredIcon
                        rules={[
                            { required: true, message: `${t("validate.input")} ${t("customerPage.form.label.address")}!`},
                        ]}
                    />
                </Col>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.status") as string}
                        name="status" 
                        placeholder={t("customerPage.form.placeholder.status") as string}
                        rules={[
                            { required: true, message: `${t("validate.select")} ${t("customerPage.form.label.status")}!`},
                        ]}
                        showRequiredIcon
                        type="select"                                    
                        options={DOCTOR_PAGE_STATUS?.map((item:any)=>({value: item.value, label: t(item.label)}))}
                    />
                </Col>
            </Row>
            <Row gutter={20}>
                <Col span={12}>
                    <CommonFormItem 
                        label={t("customerPage.form.label.note") as string}
                        name="note" 
                        placeholder={t("customerPage.form.placeholder.note") as string}
                        type='textArea'
                        rows={5}
                    />
                </Col>
            </Row>
        </CommonForm>
    )
}

export default CustomerInfoTab