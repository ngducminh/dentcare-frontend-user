import React, { useCallback, useEffect, useState } from 'react'
import { Modal, Form, notification, Tabs, TabsProps } from 'antd';
import { useTranslation } from 'react-i18next';

import { ReactComponent as UpdateIcon } from '../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../resources/images/delete.svg';

import CommonButton from '../../components/Common/Button';
import CustomerInfoTab from './CustomerInfoTab';


import userServices from "../../services/users.service";
import AccountInfoTab from './AccountInfoTab';
import { ReactComponent as Logo } from "../../resources/images/logo_small.svg";


interface CustomersCreateModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}

function CustomersCreateModal(props: CustomersCreateModalProps) {
    const [activeKey, setActiveKey] = useState<string>("1");
    const [items, setItems] = useState<TabsProps["items"]>([]);
    const [formAccountInfo] = Form.useForm();
    const [formCustomerInfo] = Form.useForm();
    const { modalStatus, handleOk, handleCancel, dataDetail } = props;
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [isSubmitAccountSuccess, setIsSubmitAccountSuccess] = useState<boolean>(false);
    const [isSubmitCustomerSuccess, setIsSubmitCustomerSuccess] = useState<boolean>(false);

    const onSubmit = () => {
        setIsLoading(true);
        formAccountInfo.submit();
        formCustomerInfo.submit();
        setIsLoading(false)
    }

    const onCreateCustomer = useCallback(async () => {
        setIsLoading(true);
        if (dataDetail?.id) { //Chỉnh sửa khách hàng
            const resp = await userServices.updateUser({ ...dataDetail?.admUser, ...formAccountInfo.getFieldsValue() });
            const data = resp?.data;
            if (resp?.status === 200) {
                const respCustomer = await userServices.updateCustomer({ ...dataDetail, ...formCustomerInfo.getFieldsValue() });
                const dataCustomer = respCustomer?.data;
                if (respCustomer?.status === 200) {
                    notification.success({
                        message: t('customerPage.message.editSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: dataCustomer?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitCustomerSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitCustomerSuccess(false);
                setIsLoading(false);
            }
        } else { // Thêm mới khách hàng
            const resp = await userServices.createUser({ ...formAccountInfo.getFieldsValue() });
            const data = resp?.data;
            if (resp?.status === 200) {
                const respCustomer = await userServices.createCustomer({ ...formCustomerInfo.getFieldsValue(), admUser: { id: data?.data?.id } });
                const dataCustomer = respCustomer?.data;
                if (respCustomer?.status === 200) {
                    notification.success({
                        message: t('customerPage.message.createSuccess'),
                    });
                    handleOk();
                } else {
                    notification.error({
                        message: dataCustomer?.message || t('commonError.oopsSystem'),
                    });
                    setIsSubmitAccountSuccess(false);
                    setIsSubmitCustomerSuccess(false);
                    setIsLoading(false);
                }
            } else {
                notification.error({
                    message: data?.message || t('commonError.oopsSystem'),
                });
                setIsSubmitAccountSuccess(false);
                setIsSubmitCustomerSuccess(false);
                setIsLoading(false);
            }
        }
        setIsSubmitAccountSuccess(false);
        setIsSubmitCustomerSuccess(false);
        setIsLoading(false);
    }, [dataDetail, formAccountInfo, formCustomerInfo, handleOk, t])

    useEffect(() => {
        console.log("isSubmitAccountSuccess", isSubmitAccountSuccess);
        console.log("isSubmitCustomerSuccess", isSubmitCustomerSuccess);
        if (isSubmitAccountSuccess && isSubmitCustomerSuccess) {
            onCreateCustomer();
        }
        if (!isSubmitCustomerSuccess) {
            setActiveKey("1")
        } else if (!isSubmitAccountSuccess) {
            setActiveKey("2")
        }
    }, [isSubmitAccountSuccess, isSubmitCustomerSuccess, onCreateCustomer])

    

    console.log("dataDetail", dataDetail)

    const onChangeTab = (key: string) => {
        setActiveKey(key);
    }

    // render tabs item
    useEffect(() => {
        const listItem = [{
            key: "1",
            label: t("customerPage.form.tabs.customerInfo") as string,
            closable: false,
            children: <CustomerInfoTab
                dataDetail={dataDetail}
                formCustomerInfo={formCustomerInfo}
                onCreateCustomer={setIsSubmitCustomerSuccess}
            />
        },
        {
            key: "2",
            label: t("customerPage.form.tabs.accountInfo") as string,
            closable: false,
            children: <AccountInfoTab
                dataDetail={dataDetail?.admUser}
                formAccountInfo={formAccountInfo}
                onCreateAccount={setIsSubmitAccountSuccess}
            />
        }];
        setItems(listItem);
    }, [dataDetail, formAccountInfo, formCustomerInfo, t]);

    return <Modal
        className="customer-modal-create"
        title={
            <>
                <div className="text-center">
                    <Logo className="style-size-logo" />
                </div><h2 className="txt-title text-center">Dentcare</h2>
            </>
        }
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={[
            <CommonButton
                key="close"
                onClick={handleCancel}
                size="small"
            >
                {t("common.button.close")}
            </CommonButton>,
            <CommonButton
                key="submit"
                htmlType="submit"
                btnType="primary"
                size="small"
                onClick={onSubmit}
                className="btn-icon-left"
                loading={isLoading}
                style={{ background: '#06A3DA', borderColor: '#06A3DA' }}
            >
                {dataDetail?.id ? <><UpdateIcon style={{ background: '#06A3DA' }} /> {t("common.button.update")}</> : <><AddIcon style={{ background: '#06A3DA' }} /> {t("common.button.addNew")}</>}
            </CommonButton>
        ]}
    >
        <Tabs
            items={items}
            activeKey={activeKey}
            onChange={onChangeTab}
        />
    </Modal>
}

export default CustomersCreateModal;

