import Carousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import React from "react";
import { useTranslation } from 'react-i18next';

const Home: React.FC = () => {
    const { t } = useTranslation();
    const priceOwlCarouselOptions = {
        autoplay: true,
        smartSpeed: 1500,
        margin: 45,
        dots: false,
        loop: true,
        nav: true,
        navText: [
            '<i className="bi bi-arrow-left"></i>',
            '<i className="bi bi-arrow-right"></i>',
        ],
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
            },
        },
    };
    const testomonialOwlCarouselOptions = {
        autoplay: true,
        autoplayTimeout: 3000,
        items: 1,
        dots: false,
        loop: true,
        nav: true,
        navText: [
            '<i class="bi bi-arrow-left"></i>',
            '<i class="bi bi-arrow-right"></i>',
        ],
    };

    return (
        <div>
            <div className="container-fluid p-0">
                <div id="header-carousel" className="carousel slide carousel-fade" data-bs-ride="carousel">
                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img className="w-100" src="img/carousel-1.jpg" alt="Image" />
                            <div className="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div className="p-3" style={{ maxWidth: "900px" }}>
                                    <h5 className="text-white text-uppercase mb-3 animated slideInDown" style={{ fontFamily: "sans-serif"}}>{t('homeApp.introHomeApp.intro1')}</h5>
                                    <h1 className="display-1 text-white mb-md-4 animated zoomIn" style={{ fontFamily: "sans-serif"}}>{t('homeApp.introHomeApp.intro2')}</h1>
                                    <a href="/appointment" className="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft" style={{ fontFamily: "sans-serif"}}>{t('navbar.appointment')}</a>
                                    <a href="/contact" className="btn btn-secondary py-md-3 px-md-5 animated slideInRight" style={{ fontFamily: "sans-serif"}}>{t('homeApp.introHomeApp.contactUs')}</a>
                                </div>
                            </div>
                        </div>
                        <div className="carousel-item">
                            <img className="w-100" src="img/carousel-2.jpg" alt="Image" />
                            <div className="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                <div className="p-3" style={{ maxWidth: "900px" }}>
                                    <h5 className="text-white text-uppercase mb-3 animated slideInDown" style={{ fontFamily: "sans-serif"}}>{t('homeApp.introHomeApp.intro1')}</h5>
                                    <h1 className="display-1 text-white mb-md-4 animated zoomIn" style={{ fontFamily: "sans-serif"}}>{t('homeApp.introHomeApp.intro2')}</h1>
                                    <a href="/appointment" className="btn btn-primary py-md-3 px-md-5 me-3 animated slideInLeft" style={{ fontFamily: "sans-serif"}}>{t('navbar.appointment')}</a>
                                    <a href="/contact" className="btn btn-secondary py-md-3 px-md-5 animated slideInRight" style={{ fontFamily: "sans-serif"}}>{t('homeApp.introHomeApp.contactUs')}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button className="carousel-control-prev" type="button" data-bs-target="#header-carousel"
                        data-bs-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="visually-hidden"></span>
                    </button>
                    <button className="carousel-control-next" type="button" data-bs-target="#header-carousel"
                        data-bs-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="visually-hidden"></span>
                    </button>
                </div>
            </div>

            <div className="container-fluid banner mb-5">
                <div className="container">
                    <div className="row gx-0">
                        <div className="col-lg-4 wow zoomIn px-2" data-wow-delay="0.1s">
                            <div className="bg-primary d-flex flex-column p-5" style={{ height: "300px" }}>
                                <h3 className="text-white mb-3">Opening Hours</h3>
                                <div className="d-flex justify-content-between text-white mb-3">
                                    <h6 className="text-white mb-0">Mon - Fri</h6>
                                    <p className="mb-0"> 8:00am - 9:00pm</p>
                                </div>
                                <div className="d-flex justify-content-between text-white mb-3">
                                    <h6 className="text-white mb-0">Saturday</h6>
                                    <p className="mb-0"> 8:00am - 7:00pm</p>
                                </div>
                                <div className="d-flex justify-content-between text-white mb-3">
                                    <h6 className="text-white mb-0">Sunday</h6>
                                    <p className="mb-0"> 8:00am - 5:00pm</p>
                                </div>
                                <a className="btn btn-light" href="">Appointment</a>
                            </div>
                        </div>
                        <div className="col-lg-4 wow  px-2" data-wow-delay="0.3s">
                            <div className="bg-dark d-flex flex-column p-5" style={{ height: "300px" }}>
                                <div className="rounded-top overflow-hidden">
                                    <img className="img-fluid" src="img/cert-receive.jpg" alt="" />
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 wow zoomIn px-2" data-wow-delay="0.6s">
                            <div className="bg-secondary d-flex flex-column p-5" style={{ height: "300px" }}>
                                <h3 className="text-white mb-3">Make Appointment</h3>
                                <p className="text-white">The pain was very great, and the pains and pains of the people were very great.</p>
                                <p className="text-white">Don't let your smile make you lose your confidence.</p>
                                <p className="text-white">Pick up the phone and call us.</p>
                                <h2 className="text-white mb-0">024 8531235</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container">
                    <div className="row g-5">
                        <div className="col-lg-7">
                            <div className="section-title mb-4">
                                <h5 className="position-relative d-inline-block text-primary text-uppercase">About Us</h5>
                                <h1 className="display-5 mb-0">The World's Best Dental Clinic That You Can Trust</h1>
                            </div>
                            <h4 className="text-body fst-italic mb-4">We always bring the best services and experiences to customers when using our services.</h4>
                            <p className="mb-4">With a team of experienced doctors along with modern equipment and professional, scientific working processes, Dentcare Dental Clinic always brings the best services to customers.</p>
                            <div className="row g-3">
                                <div className="col-sm-6 wow zoomIn" data-wow-delay="0.3s">
                                    <h5 className="mb-3"><i className="fa fa-check-circle text-primary me-3"></i>Award Winning</h5>
                                    <h5 className="mb-3"><i className="fa fa-check-circle text-primary me-3"></i>Professional Staff</h5>
                                </div>
                                <div className="col-sm-6 wow zoomIn" data-wow-delay="0.6s">
                                    <h5 className="mb-3"><i className="fa fa-check-circle text-primary me-3"></i>24/7 Opened</h5>
                                    <h5 className="mb-3"><i className="fa fa-check-circle text-primary me-3"></i>Fair Prices</h5>
                                </div>
                            </div>
                            <a href="/appointment" className="btn btn-primary py-3 px-5 mt-4 wow zoomIn" data-wow-delay="0.6s">Make Appointment</a>
                        </div>
                        <div className="col-lg-5" style={{ minHeight: "500px" }}>
                            <div className="position-relative h-100">
                                <img className="position-absolute w-100 h-100 rounded wow zoomIn" data-wow-delay="0.9s" src="img/doi-ngu-bac-si.jpg" style={{ objectFit: 'cover' }} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid bg-primary bg-appointment my-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container">
                    <div className="row gx-5">
                        <div className="col-lg-6 py-5">
                            <div className="py-5">
                                <h1 className="display-5 text-white mb-4">We Are A Certified and Award Winning Dental Clinic You Can Trust</h1>
                                <p className="text-white mb-0">Eirmod sed tempor lorem ut dolores. Aliquyam sit sadipscing kasd ipsum. Dolor ea et dolore et at sea ea at dolor, justo ipsum duo rebum sea invidunt voluptua. Eos vero eos vero ea et dolore eirmod et. Dolores diam duo invidunt lorem. Elitr ut dolores magna sit. Sea dolore sanctus sed et. Takimata takimata sanctus sed.</p>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="appointment-form h-100 d-flex flex-column justify-content-center text-center p-5 wow zoomIn" data-wow-delay="0.6s">
                                <h1 className="text-white mb-4">Make Appointment</h1>
                                <div className="row g-3">
                                    <div className="col-12 col-sm-6">
                                        <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                            <i className="fa fa-check-circle text-primary me-2"></i>Sign up & Log in
                                        </h5>
                                    </div>
                                    <div className="col-12 col-sm-6">
                                        <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                            <i className="fa fa-check-circle text-primary me-2"></i>Appointment
                                        </h5>
                                    </div>
                                    <div className="col-12 col-sm-6">
                                        <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                            <i className="fa fa-check-circle text-primary me-2"></i>Staff call to confirm
                                        </h5>
                                    </div>
                                    <div className="col-12 col-sm-6">
                                        <h5 className="text-start fw-bolder" style={{ paddingLeft: '20px', paddingTop: "15px", paddingBottom: "15px", backgroundColor: '#e7e7e7' }}>
                                            <i className="fa fa-check-circle text-primary me-2"></i>Visit our facility
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container">
                    <div className="row g-5 mb-5">
                        <div className="col-lg-5 wow zoomIn" data-wow-delay="0.3s" style={{ minHeight: "400px" }}>
                            <div className="twentytwenty-container position-relative h-100 rounded overflow-hidden">
                                <img className="position-absolute w-100 h-100" src="img/before.jpg" style={{ objectFit: 'cover' }} />
                                <img className="position-absolute w-100 h-100" src="img/after.jpg" style={{ objectFit: 'cover' }} />
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <div className="section-title mb-5">
                                <h5 className="position-relative d-inline-block text-primary text-uppercase">Our Services</h5>
                                <h1 className="display-5 mb-0">We Offer The Best Quality Dental Services</h1>
                            </div>
                            <div className="row g-5">
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.6s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-1.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Cosmetic Dentistry</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.9s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-2.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Dental Implants</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row g-5 wow fadeInUp" data-wow-delay="0.1s">
                        <div className="col-lg-7">
                            <div className="row g-5">
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.3s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-3.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Dental Bridges</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.6s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-4.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Teeth Whitening</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 service-item wow zoomIn" data-wow-delay="0.9s">
                            <div className="position-relative bg-primary rounded h-100 d-flex flex-column align-items-center justify-content-center text-center p-4">
                                <h3 className="text-white mb-3">Make Appointment</h3>
                                <p className="text-white mb-3">Clita is very big kasd rebum but the real pain is just the pain is very big</p>
                                <h2 className="text-white mb-0">024 8531235</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid bg-offer my-5 py-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-7 wow zoomIn" data-wow-delay="0.6s">
                            <div className="offer-text text-center rounded p-5">
                                <h1 className="display-5 text-white">Save 30% On Your First Dental Checkup</h1>
                                <p className="text-white mb-4">But in time, the pains are very painful. The pain and the pain and the pain and the pain are just the two things. Indeed, it is true that it is true that it is with pain that the diam two lorem is great with pain but also.</p>
                                <a href="/appointment" className="btn btn-dark py-3 px-5 me-3">Appointment</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container">
                    <div className="row g-5">
                        <div className="col-lg-5">
                            <div className="section-title mb-4">
                                <h5 className="position-relative d-inline-block text-primary text-uppercase">Pricing Plan</h5>
                                <h1 className="display-5 mb-0">We Offer Fair Prices for Dental Treatment</h1>
                            </div>
                            <p className="mb-4">High quality service with extremely attractive prices will bring you the most comfortable experience when using our services</p>
                            <h5 className="text-uppercase text-primary wow fadeInUp" data-wow-delay="0.3s">Call for Appointment</h5>
                            <h1 className="wow fadeInUp" data-wow-delay="0.6s">024 38531235</h1>
                        </div>
                        <div className="col-lg-7">
                            <Carousel className="price-carousel" {...priceOwlCarouselOptions}>

                                <div className="price-item pb-4">
                                    <div className="position-relative">
                                        <img className="img-fluid rounded-top" src="img/price-1.jpg" alt="" />
                                        <div className="d-flex align-items-center justify-content-center bg-light rounded pt-2 px-3 position-absolute top-100 start-50 translate-middle" style={{ zIndex: 2 }}>
                                            <h2 className="text-primary m-0">1.870.000</h2>
                                        </div>
                                    </div>
                                    <div className="position-relative text-center bg-light border-bottom border-primary py-5 p-4">
                                        <h4>Teeth Whitening</h4>
                                        <hr className="text-primary w-50 mx-auto mt-0" />
                                        <div className="d-flex justify-content-between mb-3"><span>Modern Equipment</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <div className="d-flex justify-content-between mb-3"><span>Professional Dentist</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <div className="d-flex justify-content-between mb-2"><span>24/7 Call Support</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <a href="/appointment" className="btn btn-primary py-2 px-4 position-absolute top-100 start-50 translate-middle">Appointment</a>
                                    </div>
                                </div>

                                {/* Price Item 2 */}
                                <div className="price-item pb-4">
                                    <div className="position-relative">
                                        <img className="img-fluid rounded-top" src="img/price-2.jpg" alt="" />
                                        <div className="d-flex align-items-center justify-content-center bg-light rounded pt-2 px-3 position-absolute top-100 start-50 translate-middle" style={{ zIndex: 2 }}>
                                            <h2 className="text-primary m-0">12.400.000</h2>
                                        </div>
                                    </div>
                                    <div className="position-relative text-center bg-light border-bottom border-primary py-5 p-4">
                                        <h4>Dental Implant</h4>
                                        <hr className="text-primary w-50 mx-auto mt-0" />
                                        <div className="d-flex justify-content-between mb-3"><span>Modern Equipment</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <div className="d-flex justify-content-between mb-3"><span>Professional Dentist</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <div className="d-flex justify-content-between mb-2"><span>24/7 Call Support</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <a href="/appointment" className="btn btn-primary py-2 px-4 position-absolute top-100 start-50 translate-middle">Appointment</a>
                                    </div>
                                </div>

                                {/* Price Item 3 */}
                                <div className="price-item pb-4">
                                    <div className="position-relative">
                                        <img className="img-fluid rounded-top" src="img/price-3.jpg" alt="" />
                                        <div className="d-flex align-items-center justify-content-center bg-light rounded pt-2 px-3 position-absolute top-100 start-50 translate-middle" style={{ zIndex: 2 }}>
                                            <h2 className="text-primary m-0">500.000</h2>
                                        </div>
                                    </div>
                                    <div className="position-relative text-center bg-light border-bottom border-primary py-5 p-4">
                                        <h4>Root Canal</h4>
                                        <hr className="text-primary w-50 mx-auto mt-0" />
                                        <div className="d-flex justify-content-between mb-3"><span>Modern Equipment</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <div className="d-flex justify-content-between mb-3"><span>Professional Dentist</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <div className="d-flex justify-content-between mb-2"><span>24/7 Call Support</span><i className="fa fa-check text-primary pt-1"></i></div>
                                        <a href="/appointment" className="btn btn-primary py-2 px-4 position-absolute top-100 start-50 translate-middle">Appointment</a>
                                    </div>
                                </div>
                            </Carousel>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid bg-primary bg-testimonial py-5 my-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-7" style={{ padding: '5px' }}>
                            <Carousel className="testimonial-carousel" {...testomonialOwlCarouselOptions} style={{ padding: '20px' }}>
                                <div className="testimonial-item text-center text-white">
                                    <img className=" img-fluid img-thumbnail mx-auto rounded mb-4 zoomIn" style={{ width: '120px' }} src="img/testimonial-1.jpg" alt="" />
                                    <p className="fs-5">When coming to see the service, the clinic takes care of customers very enthusiastically. A team of excellent doctors and extremely modern medical supplies.</p>
                                    <hr className="mx-auto w-25" />
                                    <h4 className="text-white mb-0">Nguyễn Đức Minh</h4>
                                </div>
                                <div className="testimonial-item text-center text-white">
                                    <img className="img-fluid img-thumbnail mx-auto rounded mb-4 zoomIn" style={{ width: '120px' }} src="img/testimonial-2.jpg" alt="" />
                                    <p className="fs-5">When coming to see the service, the clinic takes care of customers very enthusiastically. A team of excellent doctors and extremely modern medical supplies.</p>
                                    <hr className="mx-auto w-25" />
                                    <h4 className="text-white mb-0">Nguyễn Hoàng Mỹ Anh</h4>
                                </div>
                            </Carousel>
                        </div>
                    </div>
                </div>
            </div>

            <div className="container-fluid py-5">
                <div className="container">
                    <div className="row g-5">
                        <div className="col-lg-4 wow slideInUp" data-wow-delay="0.1s">
                            <div className="section-title bg-light rounded h-100 p-5">
                                <h5 className="position-relative d-inline-block text-primary text-uppercase">Our Dentist</h5>
                                <h1 className="display-6 mb-4">Meet Our Certified & Experienced Dentist</h1>
                                <a href="/appointment" className="btn btn-primary py-3 px-5">Appointment</a>
                            </div>
                        </div>
                        <div className="col-lg-4 wow slideInUp" data-wow-delay="0.3s">
                            <div className="team-item">
                                <div className="position-relative rounded-top" style={{ zIndex: 1 }}>
                                    <img className="img-fluid rounded-top w-100" src="img/doctor-vy.jpg" alt="" />
                                    <div className="position-absolute top-100 start-50 translate-middle bg-light rounded p-2 d-flex">
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-twitter fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-facebook-f fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-linkedin-in fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-instagram fw-normal"></i></a>
                                    </div>
                                </div>
                                <div className="team-text position-relative bg-light text-center rounded-bottom p-4 pt-5">
                                    <h4 className="mb-2">Dr. Ha Tuong Vy</h4>
                                    <p className="text-primary mb-0">Implant Surgeon</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 wow slideInUp" data-wow-delay="0.6s">
                            <div className="team-item">
                                <div className="position-relative rounded-top" style={{ zIndex: 1 }}>
                                    <img className="img-fluid rounded-top w-100" src="img/doctor-trinh.jpg" alt="" />
                                    <div className="position-absolute top-100 start-50 translate-middle bg-light rounded p-2 d-flex">
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-twitter fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-facebook-f fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-linkedin-in fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-instagram fw-normal"></i></a>
                                    </div>
                                </div>
                                <div className="team-text position-relative bg-light text-center rounded-bottom p-4 pt-5">
                                    <h4 className="mb-2">Dr. Ngo Thuy Trinh</h4>
                                    <p className="text-primary mb-0">Implant Surgeon</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 wow slideInUp" data-wow-delay="0.1s">
                            <div className="team-item">
                                <div className="position-relative rounded-top" style={{ zIndex: 1 }}>
                                    <img className="img-fluid rounded-top w-100" src="img/doctor-hung.jpg" alt="" />
                                    <div className="position-absolute top-100 start-50 translate-middle bg-light rounded p-2 d-flex">
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-twitter fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-facebook-f fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-linkedin-in fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-instagram fw-normal"></i></a>
                                    </div>
                                </div>
                                <div className="team-text position-relative bg-light text-center rounded-bottom p-4 pt-5">
                                    <h4 className="mb-2">Dr. Doan Van Hung</h4>
                                    <p className="text-primary mb-0">Implant Surgeon</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 wow slideInUp" data-wow-delay="0.3s">
                            <div className="team-item">
                                <div className="position-relative rounded-top" style={{ zIndex: 1 }}>
                                    <img className="img-fluid rounded-top w-100" src="img/doctor-chi.jpg" alt="" />
                                    <div className="position-absolute top-100 start-50 translate-middle bg-light rounded p-2 d-flex">
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-twitter fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-facebook-f fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-linkedin-in fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-instagram fw-normal"></i></a>
                                    </div>
                                </div>
                                <div className="team-text position-relative bg-light text-center rounded-bottom p-4 pt-5">
                                    <h4 className="mb-2">Dr. Pham Mai Chi</h4>
                                    <p className="text-primary mb-0">Implant Surgeon</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 wow slideInUp" data-wow-delay="0.6s">
                            <div className="team-item">
                                <div className="position-relative rounded-top" style={{ zIndex: 1 }}>
                                    <img className="img-fluid rounded-top w-100" src="img/doctor-vu.jpg" alt="" />
                                    <div className="position-absolute top-100 start-50 translate-middle bg-light rounded p-2 d-flex">
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-twitter fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-facebook-f fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-linkedin-in fw-normal"></i></a>
                                        <a className="btn btn-primary btn-square m-1" href="#"><i className="fab fa-instagram fw-normal"></i></a>
                                    </div>
                                </div>
                                <div className="team-text position-relative bg-light text-center rounded-bottom p-4 pt-5">
                                    <h4 className="mb-2">Dr. Pham Quang Vu</h4>
                                    <p className="text-primary mb-0">Implant Surgeon</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home;
