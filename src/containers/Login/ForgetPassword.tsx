import React, {useState} from 'react'
import { Checkbox, Col, Form, Modal, Row, notification } from 'antd';
import { useTranslation } from 'react-i18next';

import CommonButton from '../../components/Common/Button';
import CommonForm from '../../components/Common/Form';
import { REGEX_EMAIL, REGEX_PHONE_NUMBER } from '../../utils/constants';
import CommonFormItem from '../../components/Common/FormItem';

import userServices from "../../services/users.service";
import sendMailServices from "../../services/sendMails.service";

export interface ForgetPasswordProps{
    openModal: boolean;
    handleOk: () => void;
}

function ForgetPassword(props: ForgetPasswordProps) {
    const { openModal, handleOk } = props;
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState(false);
    const [formChanged, setFormChanged] = useState(false);

    const [form] = Form.useForm();

    const onFinish = async (values:any) => {
        setIsLoading(true);
        const resp = await userServices.forgetPassword({...values, type: values.type ? 1 : 0});
        if (resp?.status === 200) {
            notification.success({
                message: t("forgetPassword.message.forgetPasswordSuccess"),
            });
            sendMailServices.createSendMail({
                recipient: form.getFieldValue("email"),
                subject: t('sendEmail.title'),
                msgBody: `
                    ${t('sendEmail.restPassword')} 123456aA@
                    ${t('sendEmail.footer')}
                    ${t('sendEmail.address')}
                `,
            });
            handleOk();
        } 
        else {
            notification.error({
                message: resp?.data?.message || t('commonError.oopsSystem'),
            });
        }
        setIsLoading(false);
    };

    const onValuesChange = (changedValues: any) => {
        setFormChanged(true);
    }

    const onCancel = () => {
        if (formChanged) {
            Modal.confirm({
                title: t('common.confirmAction'),
                okText: t('common.button.accept'),
                onOk: async () => {
                    form.resetFields();
                    setFormChanged(false);
                    handleOk();
                },
                onCancel: () => {
                    console.log("Cancel");
                },
                cancelText: t('common.button.cancel')
            })
        }else{
            handleOk();
        }
    }

    return (
        <Modal
            className="modal-change-password"
            title={t("forgetPassword.title")}
            open={openModal}
            maskClosable={false}
            onCancel={onCancel}
            footer={[
                <CommonButton
                    key="cancel"
                    onClick={onCancel}
                    size="small"
                >
                    {t("common.button.cancel")}
                </CommonButton>,
                <CommonButton
                    key="submit"
                    htmlType="submit"
                    btnType="primary"
                    size="small"
                    className="btn-icon-left"
                    loading={isLoading}
                    onClick={()=>form.submit()}
                    style={{ backgroundColor: "#1677ff", borderColor: "#1890ff" }}
                >
                    {t("common.button.confirm")}
                </CommonButton>
            ]}
        >
            <CommonForm
                    form={form}
                    layout="vertical"
                    name="change-password"
                    requiredMark={false}
                    onFinish={onFinish}
                    onValuesChange={onValuesChange}
                    scrollToFirstError={{ behavior: 'smooth'}}
                    initialValues={{
                        type: true,
                    }}
                >
                    <Row>
                        <Col span={24}>
                            <CommonFormItem
                                name="username"
                                label={t("forgetPassword.label.username")}
                                rules={[
                                    { required: true, message: `${t("validate.input")} ${t("forgetPassword.label.username")}`}
                                ]}
                                placeholder={t("forgetPassword.placeholder.username") as string}
                            />
                        </Col>
                        <Col span={24}>
                            <CommonFormItem
                                name="email"
                                label={t("forgetPassword.label.email")}
                                rules={[
                                    { required: true, message: `${t("validate.input")} ${t("forgetPassword.label.email")}`},
                                    { pattern: REGEX_EMAIL, message: t("validate.emailRegex")}
                                ]}
                                placeholder={t("forgetPassword.placeholder.email") as string}
                            />
                        </Col>
                        <Col span={24}>
                            <CommonFormItem
                                name="phoneNumber"
                                label={t("forgetPassword.label.phoneNumber")}
                                rules={[
                                    { required: true, message: `${t("validate.input")} ${t("forgetPassword.label.phoneNumber")}`},
                                    { pattern: new RegExp(REGEX_PHONE_NUMBER), message: t('validate.phoneNumberFormat') as string },
                                ]}
                                maxLength={10}
                                placeholder={t("forgetPassword.placeholder.phoneNumber") as string}
                            />
                        </Col>
                        {/* <Col span={24}>
                            <Form.Item name="type" valuePropName="checked">
                                <Checkbox>{t("forgetPassword.label.isDoctor")}</Checkbox>
                            </Form.Item>
                        </Col> */}
                    </Row>
            </CommonForm>
        </Modal>
    );
}

export default ForgetPassword;

