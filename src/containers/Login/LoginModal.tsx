import React, { useCallback, useEffect, useRef, useState } from 'react'
import { Modal, Form, notification, Tabs, TabsProps } from 'antd';
import { useTranslation } from 'react-i18next';

import { ReactComponent as UpdateIcon } from '../../resources/images/update_icon.svg';
import { ReactComponent as AddIcon } from '../../resources/images/plus.svg';
import { ReactComponent as DeleteIcon } from '../../resources/images/delete.svg';

import CommonButton from '../../components/Common/Button';
import LocalStorage from "../../utils/localStorage";
import { LANGUAGE_LIST } from "../../utils/constants";
import { saveProfile } from "../../redux/actions/profile.actions";

import userServices from "../../services/users.service";
import CustomerInfoTab from '../Customers/CustomerInfoTab';
import AccountInfoTab from '../Customers/AccountInfoTab';
import { ReactComponent as Logo } from "../../resources/images/logo_small.svg";
import { useDispatch } from 'react-redux';
import { Button, Checkbox, Input, Select, Space } from 'antd';

import background from "../../resources/images/login/background-login.jpg";

import ForgetPassword from './ForgetPassword';


interface LoginModalProps {
    dataDetail: any,
    modalStatus: boolean,
    handleOk: () => void,
    handleCancel: () => void
}
const localLanguage = LocalStorage.getInstance().read('language');
function LoginModal(props: LoginModalProps) {
    const { modalStatus, handleOk, handleCancel, dataDetail } = props;
    const { t } = useTranslation();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const dispatch = useDispatch();
    const formRef = useRef<any>()
    const [currentLanguage, setCurrentLanguage] = useState(localLanguage || LANGUAGE_LIST[0]?.value)
    const [showModal, setShowModal] = useState<boolean>(false)

    useEffect(() => {
        dispatch(saveProfile(null))
    }, [dispatch])

    const onFinish = async (values: any) => {
        setIsLoading(true);
        const resp = await userServices.login(values);
        const data = resp?.data;
        if (resp?.status === 200) {
            LocalStorage.getInstance().save('accessToken', data?.data?.accessTokenInfo?.accessToken);
            getProfile()
        } else {
            if (formRef?.current) {
                formRef?.current?.setFields([
                    {
                        name: 'username',
                        errors: [''],
                    },
                    {
                        name: 'password',
                        errors: [t("login.wrongPassword")],
                    },
                ]);
            }
        }
        setIsLoading(false);
    };

    const getProfile = async () => {
        const resp = await userServices.getProfile();
        const data = resp?.data;
        if (resp?.status === 200) {
            let respInfo;
            if (data?.data?.permission !== 3) {
                LocalStorage.getInstance().save('accessToken', null);
                notification.error({
                    message: t("login.customerPermission")
                })
                return;
            } else {
                respInfo = await userServices.getListAccounts({
                    page: 0,
                    size: 1000,
                    search: JSON.stringify({
                        searchType: 2,
                    }),
                })
            }
            const dataInfo = respInfo?.data;
            if (respInfo?.status === 200) {
                const userInfo = dataInfo?.data?.content?.find((item: any) => item?.admUser?.id === data?.data?.id)
                dispatch(saveProfile(userInfo))
                const redirectUrl = LocalStorage.getInstance().read('redirectUrl');
                if (redirectUrl) {
                    LocalStorage.getInstance().save('redirectUrl', null);
                    window.location.href = redirectUrl
                } else {
                    window.location.reload()
                }
            }
        }
    }

    const onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };

    const handleChangeLanguage = (value: string) => {
        setCurrentLanguage(value)
        LocalStorage.getInstance().save('language', value);
        window.location.reload()
    };

    const onSubmit = () => {
        setIsLoading(true);
        window.localStorage.setItem("accessToken", "true")
        setIsLoading(false)
    }

    return <Modal
        className="customer-modal-login"
        open={modalStatus}
        maskClosable={false}
        onCancel={handleCancel}
        footer={null}
    >
        <div className="login-box">
            <div className="text-center">
                <Logo className="style-size-logo" />
            </div>
            <h2 className="txt-title text-center">Dentcare</h2>
            <Form
                ref={formRef}
                name="basic"
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
                size={'large'}
            >
                <Form.Item
                    name="username"
                    validateTrigger={"onBlur"}
                    rules={[{ required: true, message: t("validate.usernameRequired") as string }]}
                >
                    <Input
                        placeholder={t("login.usernamePlaceholder") as string}
                        allowClear
                        maxLength={50}
                    />
                </Form.Item>
                <Form.Item
                    name="password"
                    validateTrigger={"onBlur"}
                    rules={[{ required: true, message: t("validate.passwordRequired") as string }]}
                >
                    <Input.Password
                        placeholder={t("login.passwordPlaceholder") as string}
                        maxLength={20}
                    />
                </Form.Item>

                <Space style={{ justifyContent: "space-between", display: "flex", marginBottom:'2px' }}>
                    <Form.Item name="remember" valuePropName="checked">
                        <Checkbox>{t("login.rememberLabel")}</Checkbox>
                    </Form.Item>
                    <Form.Item name="forgetPassword">
                        <div className="forget-password" style={{ textDecoration: 'underline', color: '#1677ff', cursor: 'pointer' }} onClick={() => setShowModal(true)}>{t("login.forgetPassword")}</div>
                    </Form.Item>
                </Space>

                <div className="text-center">
                    <Button type="primary" className="btn-login" htmlType="submit" loading={isLoading} style={{ width: '100%', marginBottom: '10px'}}>
                        {t("login.submit")}
                    </Button>
                </div>

                {/* <div className="text-center">
                    <Select
                        value={currentLanguage}
                        style={{ width: '100%', marginBottom: '10px' }}
                        onChange={handleChangeLanguage}
                        options={LANGUAGE_LIST}
                
                        
                    />
                </div> */}
            </Form>
        </div>
        { showModal ? 
                <ForgetPassword
                    openModal={showModal}
                    handleOk={()=>setShowModal(false)}
                />
                :<></>
            }
    </Modal>
}

export default LoginModal;

