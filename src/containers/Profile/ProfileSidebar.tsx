import React, { useState } from 'react'
import {Avatar} from 'antd';
import {UserOutlined} from "@ant-design/icons";
import moment from "moment";
import {DOB_FORMAT} from "../../utils/constants";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";

import Camera from "./CameraAntd";
import CommonSpin from '../../components/Common/Spin';

function ProfileSidebar() {
    const { t } = useTranslation();
    const {
        profile
    } = useSelector((state:any) => state?.profileReducer);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    console.log("avatar", profile)

    return <div className="profile-page">
        <div className="profile-page-sidebar">
            <div className="avatar-box-container">
                <CommonSpin isLoading={isLoading}>
                    <div className="avatar-box">
                        {
                            profile?.admUser?.avatar
                                ? <Avatar className="avatar" src={profile?.admUser?.avatar} icon={<UserOutlined />} />
                                : <Avatar className="avatar" icon={<UserOutlined />} />
                        }
                        <Camera setIsLoading={setIsLoading}/>
                    </div>
                </CommonSpin>
            </div>


            <div className="profile-info">
                {/*<div className="profile-info-title">Thông tin tài khoản</div>*/}
                <div className="profile-info-full-name">{profile?.fullName}</div>
                {/* <div className="profile-info-role">{userRoleList[0]?.position?.positionName}</div> */}
                <div className="profile-info-row">
                    <div>
                        <span className="profile-info-account-label">{t("profilePage.label.dateOfBirth")}</span>
                        <span className="profile-info-account-value">{moment(profile?.dob).format(DOB_FORMAT)}</span>
                    </div>
                </div>
                <div className="profile-info-row">
                    <div>
                        <span className="profile-info-account-label">{t("profilePage.label.phoneNumber")}</span>
                        <span className="profile-info-account-value">{profile?.phoneNumber}</span>
                    </div>
                </div>
                <div className="profile-info-row">
                    <div>
                        <span className="profile-info-account-label">{t("profilePage.label.email")}</span>
                        <span className="profile-info-account-value">{profile?.email}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default ProfileSidebar;

