import React, {useEffect, useState, useRef} from 'react'
import {useTranslation} from "react-i18next";
import {Modal, Tabs} from 'antd';
import type { TabsProps } from 'antd';
import AccountProfile from "./AccountProfile";
import AccountHistory from "./AccountHistory";
import {useDispatch} from 'react-redux';
import {openGlobalDialog} from "../../../redux/actions/global.actions";

function Profile (props:any) {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const accountProfileRef = useRef<any>(null);
    const accountConfigRef = useRef<any>(null);

    const [activeKey, setActiveKey] = useState('account-profile')

    useEffect(() => {

    }, [])

    const items: TabsProps['items'] = [
        {
            key: 'account-profile',
            label: t('profilePage.accountProfileTab'),
            children: <AccountProfile ref={accountProfileRef}/>,
        },
        {
            key: 'account-history',
            label: t('profilePage.accountActionLogsTab'),
            children: <AccountHistory />,
        },
    ];

    const onChange = (key: string) => {
        // setActiveKey(key)
    };

    const onTabClick = (key: string) => {
        if (key !== activeKey) {
            if (accountProfileRef?.current?.isEdit || accountConfigRef?.current?.isEdit) {
                // dispatch(openGlobalDialog({
                //     globalDialogTitle: 'Bạn có chắc muốn tiếp tục thao tác này?',
                //     globalDialogContent: 'Bạn đang có hành động chỉnh sửa dữ liệu, hủy bỏ chỉnh sửa và tiếp túc thao tác.',
                //     globalDialogHandleOk: () => {
                //         if (accountProfileRef?.current?.isEdit && accountProfileRef?.current?.onClear) {
                //             accountProfileRef?.current?.onClear()
                //         }
                //         if (accountConfigRef?.current?.isEdit && accountConfigRef?.current?.onClear) {
                //             accountConfigRef?.current?.onClear()
                //         }
                //         setActiveKey(key)
                //     }
                // }))
                Modal.confirm({
                    title: t('common.confirmAction'),
                    content: t('common.editDataAction'),
                    centered: true,
                    okText: t('common.button.accept'),
                    onOk: () => {
                        if (accountProfileRef?.current?.isEdit && accountProfileRef?.current?.onClear) {
                            accountProfileRef?.current?.onClear()
                        }
                        if (accountConfigRef?.current?.isEdit && accountConfigRef?.current?.onClear) {
                            accountConfigRef?.current?.onClear()
                        }
                        setActiveKey(key)
                    },
                    cancelText: t('common.button.cancel')
                })
            } else {
                setActiveKey(key)
            }
        }
    };

    return <div className="profile-body">
        <Tabs activeKey={activeKey} items={items} onChange={onChange} onTabClick={onTabClick} />
    </div>
}

export default Profile;

