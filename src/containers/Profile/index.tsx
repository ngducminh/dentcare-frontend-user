import React, { useEffect, useState } from 'react'
import { useQuery } from '../../utils/customHooks'
import { useTranslation } from "react-i18next";
import LocalStore from "../../utils/localStorage";
import ProfileSidebar from "./ProfileSidebar";
import ProfileTabs from "./ProfileTabs";
import { Row, Col } from "antd";

function Profile() {
    const { t } = useTranslation();

    const queryObj: any = useQuery();
    const { params = {} } = queryObj
    const {
        tab: tabQuery,
    } = params

    useEffect(() => {

    }, [])

    return <Row className="profile-page mt-10 mb-5" style={{ marginLeft:'15px'}} >
        <div className="container-fluid bg-primary py-5 hero-header mb-5 mt-5">
            <div className="row py-3">
                <div className="col-12 text-center">
                    <h1 className="display-3 text-white animated zoomIn">Profile</h1>
                    <a href="/" className="h4 text-white">Home</a>
                    <i className="far fa-circle text-white px-2"></i>
                    <a href="/profile" className="h4 text-white">Profile</a>
                </div>
            </div>
        </div>
        <Col span={6}>
            <ProfileSidebar />
        </Col>
        <Col span={18}>
            <ProfileTabs />
        </Col>
    </Row>
}

export default Profile;

