import React from "react";

function Service() {
    return (
        <div>
            <div className="container-fluid bg-primary py-5 hero-header mb-5 mt-5">
                <div className="row py-3">
                    <div className="col-12 text-center">
                        <h1 className="display-3 text-white animated zoomIn">Services</h1>
                        <a href="/" className="h4 text-white">Home</a>
                        <i className="far fa-circle text-white px-2"></i>
                        <a href="/service" className="h4 text-white">Services</a>
                    </div>
                </div>
            </div>
            <div className="container-fluid py-5 wow fadeInUp" data-wow-delay="0.1s">
                <div className="container">
                    <div className="row g-5 mb-5">
                        <div className="col-lg-5 wow zoomIn" data-wow-delay="0.3s" style={{ minHeight: '400px' }}>
                            <div className="twentytwenty-container position-relative h-100 rounded overflow-hidden">
                                <img className="position-absolute w-100 h-100" src="img/after.jpg" style={{ objectFit: 'cover' }} />
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <div className="section-title mb-5">
                                <h5 className="position-relative d-inline-block text-primary text-uppercase">Our Services</h5>
                                <h1 className="display-5 mb-0">We Offer The Best Quality Dental Services</h1>
                            </div>
                            <div className="row g-5">
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.6s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-1.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Dental Filling</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.9s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-2.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Dental Implants</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row g-5 mb-5">
                        <div className="col-lg-5 wow zoomIn" data-wow-delay="0.3s" style={{ minHeight: '400px' }}>
                            <div className="twentytwenty-container position-relative h-100 rounded overflow-hidden">
                                <img className="position-absolute w-100 h-100" src="img/Thuy-Tien.jpg" style={{ objectFit: 'cover' }} />
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <div className="row g-5">
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.6s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-3.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Dental Bridges</h5>
                                    </div>
                                </div>
                                <div className="col-md-6 service-item wow zoomIn" data-wow-delay="0.9s">
                                    <div className="rounded-top overflow-hidden">
                                        <img className="img-fluid" src="img/service-4.jpg" alt="" />
                                    </div>
                                    <div className="position-relative bg-light rounded-bottom text-center p-4">
                                        <h5 className="m-0">Teeth Whitening</h5>
                                    </div>
                                </div>
                            </div>
                            <div className="section-title mt-5 mb-5">
                                <div className="position-relative bg-primary rounded h-100 d-flex flex-column align-items-center justify-content-center text-center p-4">
                                    <h3 className="text-white mb-3">Make Appointment</h3>
                                    <p className="text-white mb-3">Clita is very big kasd rebum but the real pain is just the pain is very big</p>
                                    <h2 className="text-white mb-0">024 8531235</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Service