import React from 'react';
import Carousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

const Testimonial: React.FC = () => {
    const testomonialOwlCarouselOptions = {
        autoplay: true,
        autoplayTimeout: 3000,
        items: 1,
        dots: false,
        loop: true,
        nav: true,
        navText: [
            '<i class="bi bi-arrow-left"></i>',
            '<i class="bi bi-arrow-right"></i>',
        ],
    };

    return (
        <div>
            <div className="container-fluid bg-primary py-5 hero-header mb-5 mt-5">
                <div className="row py-3">
                    <div className="col-12 text-center">
                        <h1 className="display-3 text-white animated zoomIn">Testimonial</h1>
                        <a href="/" className="h4 text-white">Home</a>
                        <i className="far fa-circle text-white px-2"></i>
                        <a href="/testimonial" className="h4 text-white">Testimonial</a>
                    </div>
                </div>
            </div>

            <div className="container-fluid bg-primary bg-testimonial py-5 mb-5 wow fadeInUp" data-wow-delay="0.1s" style={{ marginTop: '90px' }}>
                <div className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-lg-7" style={{ padding: '5px' }}>
                            <Carousel className="testimonial-carousel" {...testomonialOwlCarouselOptions} style={{ padding: '20px' }}>
                                <div className="testimonial-item text-center text-white">
                                    <img className=" img-fluid img-thumbnail mx-auto rounded mb-4 zoomIn" style={{ width: '120px' }} src="img/testimonial-1.jpg" alt="" />
                                    <p className="fs-5">When coming to see the service, the clinic takes care of customers very enthusiastically. A team of excellent doctors and extremely modern medical supplies.</p>
                                    <hr className="mx-auto w-25" />
                                    <h4 className="text-white mb-0">Nguyễn Đức Minh</h4>
                                </div>
                                <div className="testimonial-item text-center text-white">
                                    <img className="img-fluid img-thumbnail mx-auto rounded mb-4 zoomIn" style={{ width: '120px' }} src="img/testimonial-2.jpg" alt="" />
                                    <p className="fs-5">When coming to see the service, the clinic takes care of customers very enthusiastically. A team of excellent doctors and extremely modern medical supplies.</p>
                                    <hr className="mx-auto w-25" />
                                    <h4 className="text-white mb-0">Nguyễn Hoàng Mỹ Anh</h4>
                                </div>
                            </Carousel>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Testimonial