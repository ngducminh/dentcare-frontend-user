import * as Constants from '../constants/profile.constants';

const initialState = {
  profile: null
};

export default function (state = initialState, action:any) {
  switch (action.type) {
    case Constants.SAVE_PROFILE:
      return {
        ...state,
        profile: action?.payload,
      };
    default:
      return state
  }
}