import APIService from '../utils/apiServices';

export interface AppointmentData{
    admCustomer: {
        id: number
    },
    admDoctor?: {
        id: number
    },
    admStaff?: {
        id: number
    },
    appointmentCode: string,
    bookingDate: string,
    note: string,
    services?: {
        id: number
    }[],
    status: number,
}

export interface AppointmentSearchParams {
    search?: string,
    page?: number,
    size?: number,
    sortBy?: string,
    sortType?: string,
}

class AppointmentServices extends APIService {

    async getListAppointments(params: AppointmentSearchParams) {
        let apiParams = { 
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType ||"DESC", 
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({}) 
        }
        return await this.request('GET', `public/v1/auth/getPageAppointment`, {}, {params: apiParams});
    }


    // api appointment
    async findAllAppointment() {
        return await this.request('GET', `api/v1/appointment/findAll`);
    }
    async getAppointment(appointmentId: number) {
        return await this.request('GET', `api/v1/appointment/${appointmentId}`);
    }
    async createAppointment(data: AppointmentData) {
        return await this.request('POST', `public/v1/auth/add`, data);
    }
    async updateAppointment(data: any) {
        return await this.request('PUT', `api/v1/appointment`, data);
    }
    async deleteAppointment(appointmentId:number) {
        return await this.request('DELETE', `api/v1/appointment/${appointmentId}`);
    }
    async deleteAppointments(appointmentIds:number[]) {
        return await this.request('DELETE', `api/v1/appointment/deleteIds`, appointmentIds);
    }
    
    async cancelAppointments(ids:number[]) {
        return await this.request('POST', `api/v1/appointment/cancels`, ids);
    }
    async approvalAppointments(ids:number[]) {
        return await this.request('POST', `api/v1/appointment/approvals`, ids);
    }

    async exportAppointment(params: AppointmentSearchParams) {
        return await this.request('GET', `api/v1/appointment/exportExcel`, {}, {
            responseType: 'blob',
            params: params,
        });
    }

    

}
const service = new AppointmentServices();
export default service
