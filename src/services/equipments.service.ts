import APIService from '../utils/apiServices';

export interface EquipmentData{
    description: string,
    name: string,
    price: number,
    status: number,
    type: number
}

export interface EquipmentSearchParams {
    search?: string,
    page?: number,
    size?: number,
    sortBy?: string,
    sortType?: string,
}

class EquipmentServices extends APIService {

    async getListEquipments(params: EquipmentSearchParams) {
        let apiParams = { 
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType ||"DESC", 
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({}) 
        }
        return await this.request('GET', `api/v1/equipment/getPage`, {}, {params: apiParams});
    }


    // api equipment
    async findAllEquipment() {
        return await this.request('GET', `api/v1/equipment/findAll`);
    }
    async getEquipment(equipmentId: number) {
        return await this.request('GET', `api/v1/equipment/${equipmentId}`);
    }
    async createEquipment(data: EquipmentData) {
        return await this.request('POST', `api/v1/equipment/add`, data);
    }
    async updateEquipment(data: any) {
        return await this.request('PUT', `api/v1/equipment`, data);
    }
    async deleteEquipment(equipmentId:number) {
        return await this.request('DELETE', `api/v1/equipment/${equipmentId}`);
    }
    async deleteEquipments(equipmentIds:number[]) {
        return await this.request('DELETE', `api/v1/equipment/deleteIds`, equipmentIds);
    }
    async lockEquipments(ids:number[]) {
        return await this.request('POST', `api/v1/equipment/lock`, ids);
    }
    async unlockEquipments(ids:number[]) {
        return await this.request('POST', `api/v1/equipment/unlock`, ids);
    }

    async exportEquipment(params: EquipmentSearchParams) {
        return await this.request('GET', `api/v1/equipment/exportExcel`, {}, {
            responseType: 'blob',
            params: params,
        });
    }

}
const service = new EquipmentServices();
export default service
