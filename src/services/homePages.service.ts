import APIService from '../utils/apiServices';

export interface tinhDoanhThuProps{
    from: number,
    to: number
}

export interface tinhDoanhThuTheoDichVuProps{
    from: number,
    to: number,
    userId?: number,
}

class HomePageServices extends APIService {
    async getHomePage(id: number) {
        return await this.request('GET', `api/v1/system/homePage/${id}`);
    }
    async updateHomePage(data: any) {
        return await this.request('PUT', `api/v1/system/homePage/edit`, data);
    }
    async tinhDoanhThu(params: tinhDoanhThuProps) {
        return await this.request('GET', `api/v1/system/homePage/tinh-doanh-thu`, {}, {params});
    }

    async tinhDoanhThuTheoDichVu(params: tinhDoanhThuTheoDichVuProps) {
        return await this.request('GET', `api/v1/system/homePage/tinh-doanh-thu-theo-dich-vu`, {}, {params});
    }

    async thongKeTheoTrangThai(params: tinhDoanhThuTheoDichVuProps) {
        return await this.request('GET', `api/v1/system/homePage/thong-ke-theo-trang-thai`, {}, {params});
    }
}
const service = new HomePageServices();
export default service
