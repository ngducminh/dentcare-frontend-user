import APIService from '../utils/apiServices';

export interface ServiceData{
    slg: number[],
    equipments: {
        id: number
    }[],
    service:{
        doctors: {id: number}[],
        description: string,
        image: string,
        name: string,
        price: number,
        status: number,
        type: number
    }
}

export interface ServiceSearchParams {
    search?: string,
    page?: number,
    size?: number,
    sortBy?: string,
    sortType?: string,
}

class MedicalServicesServices extends APIService {

    async getListServices(params: ServiceSearchParams) {
        let apiParams = { 
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType ||"DESC", 
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({})
        }
        return await this.request('GET', `public/v1/auth/findAllService`, {}, {params: apiParams});
    }

    // async getDataServices(params: ServiceSearchParams) {
    //     let apiParams = { 
    //         page: params.page || 0,
    //         size: params.size || 10,
    //         sortType: params.sortType ||"DESC", 
    //         sortBy: params.sortBy || "modifiedDate",
    //         search: params.search || JSON.stringify({}) 
    //     }
    //     return await this.request('GET', `public/v1/auth/findAllService`, {}, {params: apiParams});
    // }


    // api service
    async findAllService() {
        return await this.request('GET', `api/v1/service/findAll`);
    }
    async getService(serviceId: number) {
        return await this.request('GET', `api/v1/service/${serviceId}`);
    }
    async createService(data: ServiceData) {
        return await this.request('POST', `api/v1/service/add`, data);
    }
    async updateService(data: any) {
        return await this.request('PUT', `api/v1/service`, data);
    }
    async deleteService(serviceId:number) {
        return await this.request('DELETE', `api/v1/service/${serviceId}`);
    }
    async deleteServices(serviceIds:number[]) {
        return await this.request('DELETE', `api/v1/service/deleteIds`, serviceIds);
    }
    async lockServices(ids:number[]) {
        return await this.request('POST', `api/v1/service/lock`, ids);
    }
    async unlockServices(ids:number[]) {
        return await this.request('POST', `api/v1/service/unlock`, ids);
    }

    async exportService(params: ServiceSearchParams) {
        return await this.request('GET', `api/v1/service/exportExcel`, {}, {
            responseType: 'blob',
            params: params,
        });
    }

}
const service = new MedicalServicesServices();
export default service
