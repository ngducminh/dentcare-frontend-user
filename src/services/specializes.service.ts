import APIService from '../utils/apiServices';

export interface SpecializeData{
    description: string,
    name: string,
    status: number
}

export interface SpecializeSearchParams {
    search?: string,
    page?: number,
    size?: number,
    sortBy?: string,
    sortType?: string,
}

class SpecializeServices extends APIService {

    async getListSpecializes(params: SpecializeSearchParams) {
        let apiParams = { 
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType ||"DESC", 
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({}) 
        }
        return await this.request('GET', `api/v1/specialize/getPage`, {}, {params: apiParams});
    }


    // api specialize
    async findAllSpecialize() {
        return await this.request('GET', `api/v1/specialize/findAll`);
    }
    async getSpecialize(specializeId: number) {
        return await this.request('GET', `api/v1/specialize/${specializeId}`);
    }
    async createSpecialize(data: SpecializeData) {
        return await this.request('POST', `api/v1/specialize/add`, data);
    }
    async updateSpecialize(data: any) {
        return await this.request('PUT', `api/v1/specialize`, data);
    }
    async deleteSpecialize(specializeId:number) {
        return await this.request('DELETE', `api/v1/specialize/${specializeId}`);
    }
    async deleteSpecializes(specializeIds:number[]) {
        return await this.request('DELETE', `api/v1/specialize/deleteIds`, specializeIds);
    }
    async lockSpecializes(ids:number[]) {
        return await this.request('POST', `api/v1/specialize/lock`, ids);
    }
    async unlockSpecializes(ids:number[]) {
        return await this.request('POST', `api/v1/specialize/unlock`, ids);
    }

    async exportSpecialize(params: SpecializeSearchParams) {
        return await this.request('GET', `api/v1/specialize/exportExcel`, {}, {
            responseType: 'blob',
            params: params,
        });
    }

}
const service = new SpecializeServices();
export default service
