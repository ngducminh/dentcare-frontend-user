import APIService from '../utils/apiServices';

export interface LoginData {
    username: string,
    password: string
}
export interface ChangePasswordData {
    confirmPass: string,
    newPass: string,
    oldPass: string
}

export interface ResetPasswordData {
    email: string,
    phoneNumber: string,
    type: number,  // 0: nhân viên, 1: bác sĩ , 2: khách hàng
    username: string
}
export interface AccountData {
    username: string,
    password: string
}

export interface CustomerData {
    address: string,
    admUser: {
        id: number
    },
    dob: string,
    email: string,
    fullName: string,
    gender: number,
    isDelete: number,
    note: string,
    phoneNumber: string,
    staffCode: string,
    status: number
}
export interface StaffData {
    address: string,
    admUser: {
        id: number
    },
    dob: string,
    email: string,
    fullName: string,
    gender: number,
    image: string,
    isDelete: number,
    note: string,
    phoneNumber: string,
    staffCode: string,
    status: number
}
export interface DoctorData {
    ex: number[],
    specializes: {
        id: number
    }[],
    user: {
        address: string,
        admUser: {
            id: number
        },
        dob: string,
        doctorCode: string,
        email: string,
        fullName: string,
        gender: number,
        image: string,
        note: string,
        phoneNumber: string,
        status: number
    }
}

export interface UserData {
    avatar: string,
    password: string,
    permission: number,
    status: number,
    username: string
}
export interface AccountSearchParams {
    search?: string,
    page?: number,
    size?: number,
    sortBy?: string,
    sortType?: string,
}

class UserServices extends APIService {
    // đăng nhập
    async login(data: LoginData) {
        return await this.request('POST', `public/v1/auth/jwt-login`, data);
    }

    // Lấy thông tin tài khoản
    async getProfile() {
        return await this.request('GET', `api/v1/system/user/me`, {});
    }


    //đổi mật khẩu
    async changePassword(data: ChangePasswordData) {
        return await this.request('POST', `public/v1/auth/change-password`, data);
    }

    //quên mật khẩu
    async forgetPassword(data: ResetPasswordData) {
        return await this.request('POST', `public/v1/auth/reset-password`, data);
    }


    // async resetPassword(data: string) {
    //     return await this.request('POST', `public/v1/auth/adm-reset-password`, data);
    // }

    // searchType = 0 : danh sách bác sĩ
    // searchType = 1 : danh sách nhân viên
    // searchType = 2 : danh sách khách hàng
    // searchType = 4 : danh sách tài khoản
    async getListAccounts(params: AccountSearchParams) {
        let apiParams = {
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType || "DESC",
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({ searchType: 0 })
        }
        return await this.request('GET', `api/v1/system/user/getPage`, {}, { params: apiParams });
    }


    async getListDoctor(params: AccountSearchParams) {
        let apiParams = {
            page: params.page || 0,
            size: params.size || 10,
            sortType: params.sortType || "DESC",
            sortBy: params.sortBy || "modifiedDate",
            search: params.search || JSON.stringify({ searchType: 0 })
        }
        return await this.request('GET', `public/v1/auth/getPageDoctor`, {}, { params: apiParams });
    }



    // api user
    async findAllUser() {
        return await this.request('GET', `api/v1/system/user/findAll`);
    }
    async getUser(userId: number) {
        return await this.request('GET', `api/v1/system/user/${userId}`);
    }
    async createUser(data: UserData) {
        return await this.request('POST', `public/v1/auth/register`, data);
    }
    async updateUser(data: any) {
        return await this.request('PUT', `api/v1/system/user`, data);
    }
    async deleteUsers(ids: number[]) {
        return await this.request('DELETE', `api/v1/system/user/delete`, ids);
    }
    async lockUsers(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/lock`, ids);
    }
    async unlockUsers(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/unlock`, ids);
    }



    // api Bác sĩ
    async findAllDoctor() {
        return await this.request('GET', `api/v1/system/user/doctor/findAll`);
    }
    async getDoctor(doctorId: number) {
        return await this.request('GET', `api/v1/system/user/doctor/${doctorId}`);
    }
    async createDoctor(data: any) {
        return await this.request('POST', `api/v1/system/user/addDoctor`, data);
    }
    async updateDoctor(data: any) {
        return await this.request('PUT', `api/v1/system/user/editDoctor`, data);
    }
    async deleteDoctors(ids: number[]) {
        return await this.request('DELETE', `api/v1/system/user/deleteDoctor`, ids);
    }
    async lockDoctors(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/doctor/lock`, ids);
    }
    async unlockDoctors(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/doctor/unlock`, ids);
    }

    // api Nhân viên
    async findAllStaff() {
        return await this.request('GET', `api/v1/system/user/staff/findAll`);
    }
    async getStaff(staffId: number) {
        return await this.request('GET', `api/v1/system/user/staff/${staffId}`);
    }
    async createStaff(data: StaffData) {
        return await this.request('POST', `api/v1/system/user/addStaff`, data);
    }
    async updateStaff(data: any) {
        return await this.request('PUT', `api/v1/system/user/editStaff`, data);
    }
    async deleteStaffs(ids: number[]) {
        return await this.request('DELETE', `api/v1/system/user/deleteStaff`, ids);
    }
    async lockStaffs(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/staff/lock`, ids);
    }
    async unlockStaffs(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/staff/unlock`, ids);
    }


    // api Khách hàng
    async findAllCustomer() {
        return await this.request('GET', `api/v1/system/user/customer/findAll`);
    }
    async getCustomer(customerId: number) {
        return await this.request('GET', `api/v1/system/user/customer/${customerId}`);
    }
    async createCustomer(data: CustomerData) {
        return await this.request('POST', `public/v1/auth/addCustomer`, data);
    }
    async updateCustomer(data: any) {
        return await this.request('PUT', `api/v1/system/user/editCustomer`, data);
    }
    async deleteCustomers(ids: number[]) {
        return await this.request('DELETE', `api/v1/system/user/deleteCustomer`, ids);
    }
    async lockCustomers(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/customer/lock`, ids);
    }
    async unlockCustomers(ids: number[]) {
        return await this.request('POST', `api/v1/system/user/customer/unlock`, ids);
    }

    async exportUser(params: AccountSearchParams) {
        return await this.request('GET', `api/v1/system/user/exportExcel`, {}, {
            responseType: 'blob',
            params: params,
        });
    }

}
const service = new UserServices();
export default service
